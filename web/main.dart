import 'package:angular/angular.dart';

import 'package:eno_dart_components_lib/app_component.dart';

void main() {
  bootstrap(AppComponent);
}
