import 'dart:async';
import 'dart:html';
import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';
import 'package:eno_dart_components_lib/alert_dialog/alert_dialog_box_service.dart';

@Component(
    selector: 'alert-dialog-box',
    styleUrls: const ['alert_dialog_component.css'],
    templateUrl: 'alert_dialog_component.html',
    directives: const [
      CORE_DIRECTIVES,
      materialDirectives
    ],
    providers: const []
)
class AlertDialogComponent implements OnInit, OnDestroy {

  bool showDialog = false;
  String message = "";
  StreamSubscription showDialogBoxListner;
  StreamSubscription dialogBoxMessageListner;

  AlertDialogBoxService alertDialogBoxService;

  AlertDialogComponent(this.alertDialogBoxService);

  @override
  ngOnInit() {
    init();
  }

  @override
  ngOnDestroy() {
    showDialogBoxListner.cancel();
    dialogBoxMessageListner.cancel();
  }

  init(){
    showDialogBoxListner = alertDialogBoxService.showDialog.listen((showDialog){
      this.showDialog = showDialog;
      setProperty();
    });
    dialogBoxMessageListner = alertDialogBoxService.dialogMessage.listen((message){
      this.message = message;
    });
  }

  clickOk() {
    showDialog = false;
    alertDialogBoxService.okClickEventEmitter.add(true);
  }

  setProperty(){
    new Future.delayed(const Duration(milliseconds: 150), (){
      dynamic baseModel = document.querySelector('.basic-dialog');
      if(baseModel!=null){
        baseModel.parent.style.setProperty('z-index','9999');
      }
    });
  }

}