import 'package:angular/angular.dart';

@Injectable()
class AlertDialogBoxService{

  EventEmitter showDialog = new EventEmitter();
  EventEmitter dialogMessage = new EventEmitter();
  EventEmitter okClickEventEmitter = new EventEmitter();

  var okClickEventListener;

  showMaterialDialog(message, {clickOkListener}){
    showDialog.add(true);
    dialogMessage.add(message);

    if(clickOkListener!=null){
      okClickEventListener = okClickEventEmitter.listen((value) {
        clickOkListener();
        okClickEventListener.cancel();
      });
    }

  }

  hideMaterialDialog(){
    showDialog.add(false);
  }

}