import 'dart:async';
import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';
import 'package:eno_dart_components_lib/alert_dialog/alert_dialog_box_service.dart';
import 'package:eno_dart_components_lib/alert_dialog/alert_dialog_component.dart';
import 'package:eno_dart_components_lib/confirmation_dialog_box/confirmation_dialog_component.dart';
import 'package:eno_dart_components_lib/eno_dart_components_lib.dart';
import 'package:eno_dart_components_lib/confirmation_dialog_box/confirmation_dialog_service.dart';
import 'package:eno_dart_components_lib/header/header_component.dart';
import 'package:eno_dart_components_lib/loading_model/loading_service.dart';
import 'package:eno_dart_components_lib/login/login_component.dart';
import 'package:eno_dart_components_lib/models/header_autosearch/header_auto_search.dart';
import 'package:eno_dart_components_lib/models/header_autosearch/header_drawer_group.dart';
import 'package:eno_dart_components_lib/src/alert_dialog_demo/alert_dialog_demo_component.dart';
import 'package:eno_dart_components_lib/src/confirmation_dialog_demo/confirmation_dialog_demo.dart';
import 'package:eno_dart_components_lib/src/header_demo/header_demo_component.dart';
import 'package:eno_dart_components_lib/src/loading-demo/loading_demo_component.dart';
import 'package:eno_dart_components_lib/src/login_demo/login_demo_component.dart';
import 'package:eno_dart_components_lib/src/table_wizard_demo/table_wizard_demo_component.dart';
import 'package:eno_dart_components_lib/src/tree_wizard_demo/tree_wizard_demo_component.dart';
import 'package:eno_dart_components_lib/src/wizard_step_demo/wizard_step_demo_component.dart';
import 'package:eno_dart_components_lib/tree_wizard/popup_item.dart';
import 'package:eno_dart_components_lib/tree_wizard/tree_item.dart';
import 'package:eno_dart_components_lib/tree_wizard/tree_wizard_component.dart';
import 'package:eno_dart_components_lib/tree_wizard/tree_wizard_service.dart';
import 'package:eno_dart_components_lib/wizard_step_panel/wizard_step_component.dart';
import 'package:eno_dart_components_lib/wizard_step_panel/wizard_stepper_component.dart';

@Component(
  selector: 'my-app',
  styleUrls: const ['app_component.css'],
  templateUrl: 'app_component.html',
  directives: const [
    materialDirectives,
    HeaderDemoComponent,
    LoginDemoComponent,
    WizardStepDemoComponent,
    TreeWizardDemoComponent,
    AlertDialogDemoComponent,
    ConfirmationDialogDemoComponent,
    LoadingDemoComponent,
    TableWizardDemoComponent
  ],
    providers: const [materialProviders]
)
class AppComponent {

  AppComponent();

}
