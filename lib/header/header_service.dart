import 'package:angular/angular.dart';

@Injectable()
class HeaderRestService {

  EventEmitter headerTitleListener = new EventEmitter();
  EventEmitter userNameListener = new EventEmitter();
  EventEmitter firmNameListener = new EventEmitter();
  EventEmitter showHideDrawerListener = new EventEmitter();
  EventEmitter showHideSearchBoxListener = new EventEmitter();
  EventEmitter showHideUserProfileListener = new EventEmitter();

  setHeaderTitle(title){
    headerTitleListener.add(title);
  }

  showHideNavigationDrawer(bool isShow){
    showHideDrawerListener.add(isShow);
  }

  setUserName(String userName){
    userNameListener.add(userName);
  }

  setFirmName(String firmName){
    firmNameListener.add(firmName);
  }

  showHideSearchBox(bool isShow){
    showHideSearchBoxListener.add(isShow);
  }

  showHideUserProfile(bool isShow){
    showHideUserProfileListener.add(isShow);
  }

}
