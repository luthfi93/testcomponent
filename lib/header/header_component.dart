import 'dart:html';

import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';
import 'package:eno_dart_components_lib/header/header_service.dart';
import 'package:eno_dart_components_lib/models/header_autosearch/header_auto_search.dart';
import 'package:eno_dart_components_lib/models/header_autosearch/header_drawer_group.dart';

@Component(
    selector: 'header-part',
    styleUrls: const [
      'header_component.css',
      'package:angular_components/app_layout/layout.scss.css'],
    templateUrl: 'header_component.html',
    directives: const [CORE_DIRECTIVES, materialDirectives],
    providers: const [materialProviders]
)
class HeaderComponent implements OnInit{

  @ViewChild('autoSearchInput')
  MaterialAutoSuggestInputComponent autoSearchInput;

  @Input()
  String headerTitle = "";
  @Input()
  bool showUserProfile = false;

  String userNameFirstChar = '';
  String userName = '';

  @Input('userName')
  set setUseName(String userName){
    this.userName = userName;
    if(userName != "") {
      this.userNameFirstChar = userName[0].toUpperCase();
    }
  }

  @Input()
  String firmName = '';

  @Input()
  bool showDrawer = false;

  @Input()
  List<HeaderDrawerGroup> drawerOptions;

  @Input()
  bool isDrawerVisible = false;
  DrawerItem selectedDrawer;

  @Input()
  bool showFooterImage = false;
  @Input()
  bool showHeaderLogo = false;
  @Input()
  String footerImageUrl;
  @Input()
  String logoImageUrl;

  @Input()
  bool showSearchBox = false;
  String inputText = '';
  bool filterSuggestions = false;
  bool popupMatchInputWidth = false;
  bool showClearIcon = false;
  bool shouldClearOnSelection = false;
  String searchLabel = '';
  String leadingGlyph = 'search';
  String emptyPlaceholder = '';
  SelectionModel selectionModel = new SelectionModel<List>.withList(allowMulti: false);
  SelectionOptions selectionOptions = new SelectionOptions([]);
  ItemRenderer<dynamic> itemRenderer;
  bool disabledSearchBox = false;

  bool showPopup = false;

  @Input('headerAutoSearch')
  set headerAutoSearch(HeaderAutoSearch headerAutoSearch) {
    this.inputText = headerAutoSearch.inputText;
    this.filterSuggestions = headerAutoSearch.filterSuggestions;
    this.popupMatchInputWidth=headerAutoSearch.popupMatchInputWidth;
    this.showClearIcon = headerAutoSearch.showClearIcon;
    this.shouldClearOnSelection = headerAutoSearch.shouldClearOnSelection;
    this.searchLabel = headerAutoSearch.searchLabel;
    this.emptyPlaceholder = headerAutoSearch.emptyPlaceholder;
    this.selectionModel = headerAutoSearch.selectionModel;
    this.selectionOptions = headerAutoSearch.selectionOptions;
    this.itemRenderer = headerAutoSearch.itemRenderer;
  }

  bool basicPopupVisible = false;

  @Output('onClickLogout')
  EventEmitter<dynamic> logoutClick = new EventEmitter<dynamic>();

  @Output('autoSearchEnterPress')
  EventEmitter<dynamic> autoSearchEnterPress = new EventEmitter<dynamic>();

  @Output('headerTitleClick')
  EventEmitter<dynamic> headerTitleClick = new EventEmitter<dynamic>();

  @Output('drawerItemClick')
  EventEmitter<dynamic> drawerItemClick = new EventEmitter<dynamic>();

  HeaderRestService headerRestService;

  HeaderComponent(this.headerRestService){
    isDrawerVisible = showDrawer;
  }

  @override
  ngOnInit(){
    init();
  }

  init(){
    headerRestService.headerTitleListener.listen((value){
      headerTitle = value;
    });

    headerRestService.showHideDrawerListener.listen((bool value){
      showDrawer = value;
      isDrawerVisible = value;
    });

    headerRestService.userNameListener.listen((value){
      userName = value;
      if(userName != "") {
        this.userNameFirstChar = userName[0].toUpperCase();
      }
    });

    headerRestService.firmNameListener.listen((value){
      firmName = value;
    });

    headerRestService.showHideSearchBoxListener.listen((bool value){
      showSearchBox = value;
    });

    headerRestService.showHideUserProfileListener.listen((bool value){
      showUserProfile = value;
    });
  }

  void onClickLogout(){
    logoutClick.add(true);
    basicPopupVisible = false;
  }

  @HostListener('keyup', const [r'$event', r'$event.target'])
  handleKeyboardEvent(KeyboardEvent event, EventTarget target) {
      if(event.keyCode == KeyCode.ENTER && target.toString() == 'input'){
        autoSearchInput.showPopup = false;
        autoSearchEnterPress.add(autoSearchInput.inputText);
      }
  }

  void onHeaderTitleClick(){
    headerTitleClick.add(true);
  }

  void onDrawerItemClick(DrawerItem drawer){
    drawerItemClick.add(drawer);
  }

  drawerVisible(){
    isDrawerVisible = isDrawerVisible ? false : true ;
  }
}
