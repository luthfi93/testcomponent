import 'package:angular/angular.dart';

@Injectable()
class ConfirmationDialogService {

  EventEmitter showSelectionDialog = new EventEmitter();
  EventEmitter selectionDialogMessage = new EventEmitter();
  EventEmitter yesClickEventEmitter = new EventEmitter();
  EventEmitter noClickEventEmitter = new EventEmitter();

  var yesClickEventListener, noClickEventListener;

  showSelectionDialogBox(message, yesClickListener, noClickListener) {
    showSelectionDialog.add(true);
    selectionDialogMessage.add(message);

    yesClickEventListener = yesClickEventEmitter.listen((value) {
      yesClickListener();
      showSelectionDialog.add(false);
      yesClickEventListener.cancel();
      noClickEventListener.cancel();
    });

    noClickEventListener = noClickEventEmitter.listen((value) {
      noClickListener();
      showSelectionDialog.add(false);
      noClickEventListener.cancel();
      yesClickEventListener.cancel();
    });
  }

  hideSelectionDialogBox() {
    showSelectionDialog.add(false);
  }
}