import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';
import 'package:eno_dart_components_lib/confirmation_dialog_box/confirmation_dialog_service.dart';

@Component(
    selector: 'selection-dialog-box',
    styleUrls: const ['confirmation_dialog_component.css'],
    templateUrl: 'confirmation_dialog_component.html',
    directives: const [
      CORE_DIRECTIVES,
      materialDirectives
    ],
    providers: const []
)
class ConfirmationDialogComponent implements OnInit, OnDestroy {

  bool showSelectionDialog = false;
  String selectionDialogMessage = "";
  var showDialogListner;
  var dialogMessageListner;

  ConfirmationDialogService confirmationDialogService;

  ConfirmationDialogComponent(this.confirmationDialogService);

  @override
  ngOnInit() {
    init();
  }

  @override
  ngOnDestroy() {
    showDialogListner.cancel();
    dialogMessageListner.cancel();
  }

  init() {
    showDialogListner =
        confirmationDialogService.showSelectionDialog.listen((value) {
          showSelectionDialog = value;
        });
    dialogMessageListner =
        confirmationDialogService.selectionDialogMessage.listen((value) {
          selectionDialogMessage = value;
        });
  }

  yes() {
    confirmationDialogService.yesClickEventEmitter.add(true);
  }

  no() {
    confirmationDialogService.noClickEventEmitter.add(true);
  }

}