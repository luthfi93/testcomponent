import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';
import 'package:eno_dart_components_lib/alert_dialog/alert_dialog_box_service.dart';
import 'package:eno_dart_components_lib/eno_dart_components_lib.dart';

@Component(
    selector: 'alert-dialog-demo',
    styleUrls: const ['alert_dialog_demo.css'],
    templateUrl: 'alert_dialog_demo_component.html',
    directives: const [
      materialDirectives,
      enoDartComponentDirectives
    ],
    providers: const [materialProviders, enoDartComponentProviders]
)
class AlertDialogDemoComponent {
  AlertDialogBoxService alertDialogBoxService;

  AlertDialogDemoComponent(this.alertDialogBoxService);


  void showAlertDialog(){
    alertDialogBoxService.showMaterialDialog('Show Material dialog clicked', clickOkListener: () async {
      print("Click ok");
    });
  }
}