import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';
import 'package:eno_dart_components_lib/alert_dialog/alert_dialog_box_service.dart';
import 'package:eno_dart_components_lib/confirmation_dialog_box/confirmation_dialog_service.dart';
import 'package:eno_dart_components_lib/eno_dart_components_lib.dart';

@Component(
    selector: 'confirmation-dialog-demo',
    styleUrls: const ['confirmation_dialog_demo.css'],
    templateUrl: 'confirmation_dialog_demo_component.html',
    directives: const [
      materialDirectives,
      enoDartComponentDirectives
    ],
    providers: const [materialProviders, enoDartComponentProviders]
)
class ConfirmationDialogDemoComponent {

  ConfirmationDialogService confirmationDialogService;

  ConfirmationDialogDemoComponent(this.confirmationDialogService);

  void showDialog(){
    confirmationDialogService.showSelectionDialogBox(
        "Selection Dialog Box is working, right ?", () async{
      print("Yes");
    } , () async{
      print("No");
    });
  }
}