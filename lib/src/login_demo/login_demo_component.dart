import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';
import 'package:eno_dart_components_lib/eno_dart_components_lib.dart';

@Component(
    selector: 'login-demo',
    styleUrls: const [''],
    templateUrl: 'login_demo_component.html',
    directives: const [
      materialDirectives,
      enoDartComponentDirectives
    ],
    providers: const [materialProviders]
)
class LoginDemoComponent {

  String headerTitle="Calendar App";
  bool isShowHeader = true;

  void signInClick(dynamic loginData){
    print(loginData);
    /*confirmationDialogComponent.showDialogbox("Are you sure want to retrieve login values ?",(){
      print(loginData);
      print(loginData['email']);
      if(loginData['email'] == ""){
        alertDialogComponent.showDialogbox("Please enter email address ",clickListener: (){
          print('click listener');
        });
      }
    },(){
    });*/
  }

}