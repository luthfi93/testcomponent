import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';
import 'package:eno_dart_components_lib/eno_dart_components_lib.dart';
import 'package:eno_dart_components_lib/models/header_autosearch/header_auto_search.dart';
import 'package:eno_dart_components_lib/models/header_autosearch/header_drawer_group.dart';
@Component(
    selector: 'header-demo',
    styleUrls: const [''],
    templateUrl: 'header_demo_component.html',
    directives: const [
      materialDirectives,
      enoDartComponentDirectives
    ],
    providers: const [materialProviders]
)
class HeaderDemoComponent implements OnInit {

  String headerTitle="Calendar App";
  String userName = "James Smith";
  String firmName = "Wood County Court of Common Pleas";
  String footerImageUrl = "./CourtFileNow_logo.png";
  bool showSearchBox = true;
  bool showDrawer = true;
  bool showFooterImage = true;
  bool isDrawerVisible = true;
  bool showUserProfile = true;

  List<HeaderDrawerGroup> drawerOptions = [
    new HeaderDrawerGroup('', [
      new DrawerItem('home', 'Home'),
      new DrawerItem('delete', 'Delete')
    ]),
    new HeaderDrawerGroup('Tags', [ new DrawerItem('done', 'DoNe')])
  ];

  HeaderAutoSearch headerAutoSearch = new HeaderAutoSearch();

  @override
  ngOnInit() {
    headerAutoSearch.selectionOptions = getOptions();
    headerAutoSearch.popupMatchInputWidth = true;
    headerAutoSearch.showPopup = false;
  }

  SelectionOptions getOptions(){

    List<String> list = new List<String>();
    list.add("Company");
    list.add("Person");

    OptionGroup<String> optionGroup = new OptionGroup<String>(list);

    List<OptionGroup> optionList = new List<OptionGroup>();
    optionList.add(optionGroup);

    return new SelectionOptions(optionList);
  }

  void selectDrawer(DrawerItem drawerItem){
    print('drawer item click..');
  }

  void onClickLogout(){
    print('logout button click');
  }

  void onHeaderTitleClick(){
    print('header title click');
  }

  void autoSearchEnterPress(String value){
    print('enter key press..'+value);
  }

}