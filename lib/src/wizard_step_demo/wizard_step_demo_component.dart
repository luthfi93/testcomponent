import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';
import 'package:eno_dart_components_lib/eno_dart_components_lib.dart';

@Component(
    selector: 'wizard-step-demo',
    styleUrls: const [''],
    templateUrl: 'wizard_step_demo_component.html',
    directives: const [
      materialDirectives,
      enoDartComponentDirectives
    ],
    providers: const [materialProviders]
)
class WizardStepDemoComponent {

  @ViewChild(WizardStepperComponent) WizardStepperComponent wizardStepperComponent;

  void clickContinueButton(index){
    wizardStepperComponent.setActiveTab(index);
  }

  void clickStepButton(value){
    wizardStepperComponent.setSelectTab();
  }

}