import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';
import 'package:eno_dart_components_lib/eno_dart_components_lib.dart';
import 'package:eno_dart_components_lib/tree_wizard/popup_item.dart';
import 'package:eno_dart_components_lib/tree_wizard/tree_item.dart';
import 'package:eno_dart_components_lib/tree_wizard/tree_wizard_component.dart';

@Component(
    selector: 'tree-wizard-demo',
    styleUrls: const [''],
    templateUrl: 'tree_wizard_demo_component.html',
    directives: const [
      materialDirectives, CORE_DIRECTIVES,
      enoDartComponentDirectives
    ],
    providers: const [materialProviders]
)
class TreeWizardDemoComponent implements OnInit{
  @ViewChild('sample1') TreeWizardComponent treeWizardComponent;
  @ViewChild('sample2') TreeWizardComponent treeWizardComponent1;

  List<TreeItem> treeItemLabels = new List<TreeItem>();
  List<TreeItem> treeItemLabels1 = new List<TreeItem>();
  List<String> popupItemList = ["Rename", "Delete"];
  List<TreeItem> treeItemLabelList = [
    new TreeItem("Tree 1", glyph: 'folder',isExpanded:true,children: [
      new TreeItem("Tree 11",  glyph: 'more_vert'),
          new TreeItem("Tree 12",  glyph: 'more_vert')
          ]
    )];

  List<TreeItem> treeItemLabelList1 = [
    new TreeItem("Tree 1", glyph: 'folder',isExpanded:true,children: [
      new TreeItem("Tree 11",  glyph: 'more_vert', showIconRight: true),
      new TreeItem("Tree 12",  glyph: 'more_vert', showIconRight: true)
      ]
    )];


  @override
  ngOnInit() {
    treeItemLabels.add(new TreeItem( "My Case", glyph: 'folder', children: treeItemLabelList));
    treeItemLabels1.add(new TreeItem( "My Case", glyph: 'folder', children: treeItemLabelList1));

    treeWizardComponent.setTreeWizardListItem(treeItemLabels);
    treeWizardComponent1.setTreeWizardListItem(treeItemLabels1);
  }

  void showTree(){
    treeItemLabels.add(new TreeItem( "My Case", glyph: 'folder', children: treeItemLabelList));
    treeItemLabels1.add(new TreeItem( "My Case", glyph: 'folder', children: treeItemLabelList1));

    treeWizardComponent.setTreeWizardListItem(treeItemLabels);
    treeWizardComponent1.setTreeWizardListItem(treeItemLabels1);
  }

  void selectTreeItem(TreeItem treeItem){
    print('tree item click..');
  }

  void selectPopupItem(PopupItem popupItem){
    print('popup item click..');
  }


}