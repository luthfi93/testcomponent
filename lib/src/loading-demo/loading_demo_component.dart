import 'dart:async';
import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';
import 'package:eno_dart_components_lib/eno_dart_components_lib.dart';
import 'package:eno_dart_components_lib/loading_model/loading_service.dart';

@Component(
    selector: 'loading-modal-demo',
    styleUrls: const ['loading_demo.css'],
    templateUrl: 'loading_demo_component.html',
    directives: const [
      materialDirectives,
      enoDartComponentDirectives
    ],
    providers: const [materialProviders, enoDartComponentProviders]
)
class LoadingDemoComponent {

  LoadingService loadingService;

  LoadingDemoComponent(this.loadingService);

  void showLoding(){
    loadingService.startSpinner();
    new Future.delayed(const Duration(milliseconds: 2500), (){
      loadingService.stopSpinner();
    });
  }
}