import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';
import 'package:eno_dart_components_lib/table_wizard/table_wizard_component.dart';

@Component(
    selector: 'table-wizard-demo',
//    styleUrls: const ['table_wizard_demo_component.css'],
    templateUrl: 'table_wizard_demo_component.html',
    directives: const [materialDirectives, CORE_DIRECTIVES, TableWizardComponent],
    providers: const [materialProviders]
)
class TableWizardDemoComponent {
  List<String> headerList = ["Header 1", "Header 2"];
  List itemList = [{"name":"item1", "value":"value1"}, {"name":"item2", "value":"value2"}];
}
