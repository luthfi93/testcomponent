import 'package:eno_dart_components_lib/tree_wizard/tree_item.dart';

class PopupItem {
  String popupLabel;
  TreeItem treeItem;

  PopupItem(this.popupLabel, this.treeItem);
}