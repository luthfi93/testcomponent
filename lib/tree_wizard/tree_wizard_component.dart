import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';
import 'package:eno_dart_components_lib/tree_wizard/popup_item.dart';
import 'package:eno_dart_components_lib/tree_wizard/tree_item.dart';
import 'package:eno_dart_components_lib/tree_wizard/tree_wizard_service.dart';

@Component(
    selector: 'tree-wizard',
    styleUrls: const ['tree_wizard_component.css'],
    templateUrl: 'tree_wizard_component.html',
    directives: const [
      CORE_DIRECTIVES,
      materialDirectives,
      MaterialIconComponent,
      MaterialListComponent,
      MaterialListItemComponent,
      PopupSourceDirective,
      COMMON_DIRECTIVES
    ],
    providers: const [materialProviders]
)

class TreeWizardComponent {
  @Input() TreeItem treeItem;
  @Input() List<TreeItem> treeItemLabels;
  @Input() List<String> popupItemList;

  @Output() get popupItemClickEvent => popupItemListner;
  EventEmitter popupItemListner = new EventEmitter();

  @Output() get treeItemClickEvent => treeItemListner;
  EventEmitter treeItemListner = new EventEmitter();

  EventEmitter treeItemListListner = new EventEmitter();

  bool showRadioButton = false;
  final SelectionModel treeItemModel = new SelectionModel.withList();


  String itemLabel;
  bool menuOption = false;
  bool isPopup = false;
  TreeItem selectedItem;

  final treeItems = <TreeItem>[];
  final treeItemLabelList = <TreeItem>[];
  TreeItem _selected;

  setTree() {
    if(this.popupItemList!=null && this.popupItemList.isNotEmpty){
      this.isPopup = true;
    }

    if(this.treeItem!=null){
      this._traverseItems(this.treeItem);
      this.selectFolder(this.treeItem);
    }

    if(this.treeItemLabels!=null){
      for(TreeItem treeItem in this.treeItemLabels){
        this._traverseItems(treeItem);
//        this.selectFolder(treeItem);
      }
    }
  }

  void _traverseItemLabel(TreeItem item){
    this.treeItemLabelList.add(item);
    item?.children?.forEach(_traverseItemLabel);
  }

  void _traverseItems(TreeItem item) {
    this.treeItems.add(item);
    item?.children?.forEach(_traverseItems);
  }

  void selectFolder(TreeItem item) {
    this.treeItemListner.add(item);
    if (this._selected == item) {
      item.toggle();
    } else {
      this._selected = item;
    }
    this.itemLabel = item.label;
    this.selectedItem = item;

    for(TreeItem treeItem in this.treeItems){
      if(treeItem != item){
        treeItem.selected = false;
      }
    }

    for(TreeItem treeItem in this.treeItemLabelList){
      if(treeItem != item){
        treeItem.selected = false;
      }
    }
  }

  void popupIcon(item){
    if(!item.toggleVisible && item.showPopup){
      if((this.selectedItem == item || !this.menuOption) && this.isPopup){
        this.menuOption = !this.menuOption;
      }
      this.selectedItem = item;
    }
  }

  void selectPopupItem(popupLabel, item){
    PopupItem popupItem = new PopupItem(popupLabel, item);
    this.popupItemListner.add(popupItem);
    this.menuOption = false;
  }


  void setTreeWizardListItem(List<TreeItem>  value){
    setTree();

    this.treeItemLabelList.clear();
    this.treeItemLabels = value;
    for(TreeItem treeItem in this.treeItemLabels){
      _traverseItemLabel(treeItem);
    }
  }
}

