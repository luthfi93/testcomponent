import 'package:angular/angular.dart';
import 'package:eno_dart_components_lib/tree_wizard/tree_item.dart';

@Injectable()
class TreeWizardService {

  EventEmitter treeItemListner = new EventEmitter();
  EventEmitter treeItemListListner = new EventEmitter();

  setTreeWizardItem(value){
    treeItemListner.add(value);
  }

  setTreeWizardListItem(List<TreeItem>  value){
    treeItemListListner.add(value);
  }

}