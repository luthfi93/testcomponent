const String defaultIconGlyph = '';

class TreeItem {
  final String glyph;
  final String label;
  bool isExpanded;
  TreeItem parent;
  final List<TreeItem> children;
  dynamic itemObject;
  bool showRadioButton;
  bool selected;
  bool showIconRight;
  bool showPopup;

  bool get isRoot => parent == null;
  bool get isVisible => isRoot || (parent.isVisible && parent.isExpanded);

  bool get hasChildren => children?.isNotEmpty ?? false;
  bool get toggleVisible => hasChildren;
  String get toggleGlyph => isExpanded ? 'expand_more' : 'chevron_right';

  int get depth => parent == null ? 0 : parent.depth + 1;
  int get indentPx => (depth * 16) + (toggleVisible ? 0 : 40);

  TreeItem(this.label,
      {this.glyph: defaultIconGlyph, this.isExpanded: false, this.children, this.itemObject, this.showRadioButton = false, selected = false,
        this.showIconRight=false, this.showPopup = true}) {
    children?.forEach((child) {
      child.parent = this;
    });
  }

  void toggle() {
    isExpanded = !isExpanded;
  }
}