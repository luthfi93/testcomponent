import 'dart:async';
import 'dart:html';
import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';
import 'package:angular_forms/angular_forms.dart';
import 'package:eno_dart_components_lib/header/header_component.dart';
import 'package:eno_dart_components_lib/pipes/safe_html.dart';

@Component(
    selector: 'login-component',
    styleUrls: const ['login_component.css'],
    templateUrl: 'login_component.html',
    directives: const [
      CORE_DIRECTIVES,
      materialDirectives,
      HeaderComponent,
      NgForm
    ],
    providers: const [materialProviders],
    pipes: const [SafeHtmlPipe]
)
class LoginComponent{

  String email="";
  String password = "";

  @Input()
  String headerTitle='';

  @Input()
  bool isShowHeader = false;

  @Input()
  bool isShowEfspLinks = false;

  @Input()
  String efspLinksHtml='';

  @Output('signInClick')
  EventEmitter<dynamic> signInClick = new EventEmitter<dynamic>();

  void onClickSignIn(){
    signInClick.add({'email':email, 'password':password});
  }
}
