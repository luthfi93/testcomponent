import 'package:angular_components/angular_components.dart';
import 'package:angular_components/model/ui/has_renderer.dart';

class HeaderAutoSearch{
  bool shouldClearOnSelection = false;
  bool closeOnActivate = true;
  bool hideCheckbox = false;
  bool initialActivateSelection = false;
  bool filterSuggestions = true;
  String popupShadowCssClass = '';
  bool popupMatchInputWidth = false;
  String slide;
  bool showClearIcon = false;
  String emptyPlaceholder = '';
  ItemRenderer<dynamic> itemRenderer;
  ComponentRenderer componentRenderer;
  SelectionModel selectionModel =  new SelectionModel<List>.withList(allowMulti: false);
  SelectionOptions selectionOptions = new SelectionOptions([]);
  dynamic limit;
  ComponentRenderer labelRenderer;
  bool highlightOptions = true;
  bool showPopup = true;
  String inputText = '';
  String searchLabel = '';
}