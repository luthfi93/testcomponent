import 'package:angular_components/angular_components.dart';
import 'package:angular_components/model/ui/has_renderer.dart';

class HeaderDrawerGroup{
  String label;
  List<DrawerItem> drawerItemList = new List<DrawerItem>();

  HeaderDrawerGroup(this.label, this.drawerItemList);
}

class DrawerItem{
  String icon;
  String name;

  DrawerItem(this.icon, this.name);
}