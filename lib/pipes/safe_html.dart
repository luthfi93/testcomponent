import 'package:angular/angular.dart';
import 'package:angular/src/security/dom_sanitization_service.dart';


@Pipe('safeHTML')
class SafeHtmlPipe extends PipeTransform {
  DomSanitizationService domSanitizationService;

  SafeHtmlPipe(this.domSanitizationService){}

  transform(html) {
    return domSanitizationService.bypassSecurityTrustHtml(html);
  }
}