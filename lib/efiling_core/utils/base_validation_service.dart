import 'package:angular/angular.dart';

@Injectable()
class BaseValidationService{

  bool inputValidation(String inputValue){
    if(inputValue == null || inputValue.trim() == ""){
      return false;
    } else {
      return true;
    }
  }

  bool faxValidation(fax){
    if (fax == null || fax.trim() == "")
      return true;

    return phoneValidation(fax);
  }

  bool phoneValidation(String phone){

    if (phone == null || phone.trim() == "")
      return false;

    String validChars = "0123456789-() ";
    bool validNumber = true;
    String character;
    int phoneLength = phone.length;
    int i = 0;

    if (phoneLength < 10) {
      validNumber = false;
    } else if (phoneLength >= 10) {
      for (i = 0; i < phoneLength && validNumber == true; i++) {
        character = phone.substring(i, i+1);
        if (validChars.indexOf(character) == -1) {
          validNumber = false;
          return validNumber;
        }
      }
    }
    return validNumber;
  }

  bool emailValidation(String email) {
    String p = r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';

    RegExp regExp = new RegExp(p);

    return regExp.hasMatch(email);
  }

  bool zipcodeValidation(String zipcode){

    if (zipcode == null || zipcode.trim() == "")
      return false;

    return isZipcode(zipcode);
  }

  bool isZipcode(String zipcode){
    if(zipcode.length != 5){
      return false;
    }

    try{
      int.parse(zipcode);
    }catch(e){
      return false;
    }

    return true;
  }

  bool isNumeric(String s) {
    if(s == null) {
      return false;
    }

    return double.parse(s, (e) => null) != null ||
        int.parse(s, onError: (e) => null) != null;
  }

  bool digitValidator(String value, int requiredLength) {
    if (value == null)
      return false;
    String validChars = "0123456789";
    bool validValue = true;
    String character;
    int valueLength = value.length;

    if (valueLength != requiredLength) {
      validValue = false;
    } else {
      for (int i = 0; i < valueLength && validValue == true; i++) {
        character = value.substring(i, i+1);
        if (validChars.indexOf(character) == -1) {
          validValue = false;
          return validValue;
        }
      }
    }
    return validValue;
  }

  bool checkCcMonth(int month){

    if(month > 12 || month <= 0){
      return false;
    }

    return true;
  }

  bool checkCC(String ccNumber) {
    if (ccNumber == null)
      return false;
    String validChars = "0123456789";
    bool isDigitsOnly = true;
    bool isValidCc = false;
    String character;

    for (int i = 0; i < ccNumber.length && isDigitsOnly == true; i++) {
      character = ccNumber.substring(i, i+1);
      if (validChars.indexOf(character) == -1) {
        isDigitsOnly = false;
        return isDigitsOnly;
      }

    }

    if (isDigitsOnly == true) {
      isValidCc = checkLuhnAlgorithm(ccNumber);

    }
    return isValidCc;
  }

  bool checkLuhnAlgorithm(String ccNumber) {
    int sum = 0;
    int digit = 0;
    int addend = 0;
    bool timesTwo = false;

    for (int i = ccNumber.length - 1; i >= 0; i--) {
      digit = int.parse(ccNumber.substring(i, i + 1));
      if (timesTwo) {
        addend = digit * 2;
        if (addend > 9) {
          addend -= 9;
        }
      } else {
        addend = digit;
      }
      sum += addend;
      timesTwo = !timesTwo;
    }

    int modulus = sum % 10;
    return modulus == 0;
  }

  String validateBarId(String barId) {
    String responseBarId = "";

    if (barId == null || barId.trim() == "") {
      return responseBarId;
    }
    else if (!isValidBarId(barId.trim())) {
      return responseBarId;
    }
    else {
      String barid = "";
      if (barId.trim().contains("-")) {
        barid = barId.trim().replaceAll('-', '');
      } else {
        barid = barId.trim();
      }
      responseBarId = barid;
    }
    return responseBarId;
  }

  bool isValidBarId(String barId) {
    if (barId == null || barId == "") {
      return false;
    }
    String validChars = "0123456789-ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    bool validNumber = true;
    String character;
    int barIdLength = barId.length;
    int i = 0;

    if (barIdLength < 5) {
      validNumber = false;
    } else if (barIdLength >= 5) {
      for (i = 0; i < barIdLength && validNumber == true; i++) {
        character = barId[i];
        if (validChars.indexOf(character) == -1) {
          validNumber = false;
          return validNumber;
        }
      }
    }
    return validNumber;
  }

}