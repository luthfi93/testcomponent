import 'package:uuid/uuid.dart';

class GUIDGenerator{
  static String nextGUID(){
    var uuid = new Uuid();
    return uuid.v1();
  }
}