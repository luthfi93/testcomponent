import 'dart:async';
import 'package:angular/angular.dart';
import 'package:timezone/browser.dart';
import 'package:intl/intl.dart';

@Injectable()
class DateTimeService{

  Future<String> convertDateStrToEST(String dateTimeStr) async {

    dateTimeStr = dateTimeStr.replaceFirst(' pm', ' PM').replaceFirst(' am', ' AM');
    DateFormat fmt = new DateFormat("MMM dd, yyyy h:mm:ss a");
    DateTime dateTime = fmt.parse(dateTimeStr);

    DateFormat fmt1 = new DateFormat("MM/dd/yyyy h:mm a jz", "America/New_York");
    String convertedDateTimeStr = "";

    var completer = new Completer();

    await initializeTimeZone().then((_) {
      final detroit = getLocation('America/New_York');
      final now = new TZDateTime.from(dateTime, detroit);

      print("Converted");
      print(now);
      print(now.timeZoneName);

      convertedDateTimeStr = fmt1.format(dateTime);
      completer.complete(convertedDateTimeStr);
    });

    print("Return");
    return completer.future;
  }

}