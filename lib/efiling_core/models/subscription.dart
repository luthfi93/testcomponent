import 'package:exportable/exportable.dart';
import 'package:eno_dart_components_lib/efiling_core/models/domain.dart';

class Subscription extends Domain{
  @export DateTime startDate;
  @export DateTime endDate;
  @export String type = "";
}