import 'package:eno_dart_components_lib/efiling_core/models/county.dart';
import 'package:eno_dart_components_lib/efiling_core/models/domain.dart';

class CountyList extends Domain {
  List<County> countyList = new List<County>();
}