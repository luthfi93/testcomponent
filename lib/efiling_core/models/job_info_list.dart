import 'package:eno_dart_components_lib/efiling_core/models/domain.dart';
import 'package:eno_dart_components_lib/efiling_core/models/job_info.dart';
import 'package:exportable/exportable.dart';

class JobInfoList extends Domain {
  @export List<JobInfo> jobInfoList = new List<JobInfo>();
  dynamic jobInfoListJson;

  Map convertToMap(){
    Map jobInfoListMap = this.toMap();

    //convert jobInfo object list to map list
    List<Map> jobInfoMap = new List<Map>();
    this.jobInfoList.forEach((JobInfo jobInfo){
      jobInfoMap.add(jobInfo.convertToMap());
    });
    jobInfoListMap['jobInfoList'] = jobInfoMap;

    return jobInfoListMap;
  }

  JobInfoList convertFromJson(jobInfoListJson){
    JobInfoList jobInfoListObj = new JobInfoList();
    jobInfoListObj.initFromMap(jobInfoListJson);

    List<JobInfo> jobInfoList = new List<JobInfo>();
    for(dynamic jobInfoJson in jobInfoListJson['jobInfoList']){
      JobInfo jobInfo = new JobInfo();
      jobInfo = jobInfo.convertFromJson(jobInfoJson);
      jobInfoList.add(jobInfo);
    }

    jobInfoListObj.jobInfoList = jobInfoList;
    jobInfoListObj.jobInfoListJson = jobInfoListJson;
    return jobInfoListObj;
  }
}