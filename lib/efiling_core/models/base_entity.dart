import 'package:exportable/exportable.dart';

class BaseEntity extends Object with Exportable{
  @export String key = "";
  @export DateTime createdDate;
  @export String createdDateStr = "";
}