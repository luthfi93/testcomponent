import 'package:eno_dart_components_lib/efiling_core/models/address.dart';
import 'package:eno_dart_components_lib/efiling_core/models/credit_card.dart';
import 'package:eno_dart_components_lib/efiling_core/models/office.dart';
import 'package:exportable/exportable.dart';


class LawFirm extends Office {
  @export bool detailView = false;
  @export List<CreditCard> creditCards = new List<CreditCard>();
  @export String contactFirstName = "";
  @export String contactMiddleName = "";
  @export String contactLastName = "";
  @export String contactPhone = "";
  @export bool individualUser = false;

  LawFirm convertFromJson(Map lawFirmMap){
    LawFirm lawFirm = new LawFirm();
    lawFirm.initFromMap(lawFirmMap);

    if(lawFirmMap != null){
      if(lawFirmMap['address'] != null){
        lawFirm.address = address.convertFromJson(lawFirmMap['address']);
      }
    }

    return lawFirm;
  }
}
