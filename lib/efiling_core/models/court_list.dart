import 'dart:convert';
import 'package:exportable/exportable.dart';
import 'package:eno_dart_components_lib/efiling_core/models/court.dart';
import 'package:eno_dart_components_lib/efiling_core/models/domain.dart';

class CourtList extends Domain {
  @export List<Court> courtList = new List<Court>();

  convertFromJson(courtJsonList){

    CourtList courtListObject = new CourtList();
    List<Court> courtList = new List<Court>();

    for(dynamic courtJSON in courtJsonList){
      Court court = new Court();
      court = court.convertFromJSON(courtJSON);
      courtList.add(court);
    }

    courtListObject.courtList = courtList;
    return courtListObject;
  }
}