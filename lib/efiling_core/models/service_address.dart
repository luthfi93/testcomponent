import 'package:eno_dart_components_lib/efiling_core/models/address.dart';
import 'package:eno_dart_components_lib/efiling_core/models/service_method.dart';
import 'package:eno_dart_components_lib/efiling_core/models/services.dart';
import 'package:exportable/exportable.dart';

class ServiceAddress extends Address {
  @export Services services = new Services();
  @export List<String> serviceTypeList = new List<String>();


  Map convertToMap(){
    Map serviceAddressMap = this.toMap();

    List<ServiceMethod> serviceMethodList = this.serviceMethods;

    List<dynamic> serviceMethodsMap = new List<dynamic>();
    serviceMethodList.forEach((ServiceMethod serviceMethod){
      serviceMethodsMap.add(serviceMethod.convertToMap());
    });

    serviceAddressMap['serviceMethods'] = serviceMethodsMap;

    return serviceAddressMap;
  }

  ServiceAddress convertFromJson(serviceAddressJson){
    ServiceAddress serviceAddress = new ServiceAddress();
    serviceAddress.initFromMap(serviceAddressJson);

    Services services = new Services();
    if(serviceAddressJson['services'] != null) {
      services.initFromMap(serviceAddressJson['services']);
    }
    serviceAddress.services = services;

    List<String> serviceTypeList = new List<String>();
    if(serviceAddressJson != null){
      if(serviceAddressJson['serviceTypeList']){
        for(String serviceType in serviceAddressJson['serviceTypeList']){
          serviceTypeList.add(serviceType);
        }
      }
    }
    serviceAddress.serviceTypeList = serviceTypeList;

    List<ServiceMethod> serviceMethods = new List<ServiceMethod>();
    if(serviceAddressJson != null){
      if(serviceAddressJson['serviceMethods']){
        for(ServiceMethod serviceMethod in serviceAddressJson['serviceMethods']){
          serviceMethods.add(serviceMethod);
        }
      }
    }
    serviceAddress.serviceMethods = serviceMethods;

    return serviceAddress;
  }
}