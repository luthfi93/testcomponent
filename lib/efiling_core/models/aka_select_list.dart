class AkaSelectList{
  static String administratorOf = "ADM";
  static String acting = "ACTING";
  static String estateOf = "ESTATE OF";
  static String fkn = "FKN";
  static String fna = "FNA";
  static String orHis = "OR HIS";
  static String suc = "SUC";
  static String sucIn = "SUC-IN";
  static String dba = "DBA";
  static String byMerger = "BY MERGER";
  static String company = "COMPANY";
  static String fDBA = "FDBA";
  static String nbm = "NBM";
  static String sbm = "SBM";
  static String sbmt = "SBMT";
  static String tdba = "TDBA";
  static String aka = "AKA";
  static String fka = "FKA";
  static String slash = "/";
  static String amendedTo = "AMENDED TO";
  static String solely = "SOLELY";
  static String among = "AMONG";
  static String an = "AN";
  static String andAs = "AND AS";
  static String andAsThe = "AND AS THE";
  static String andOr = "AND/OR";
  static String as = "AS";
  static String between = "BETWEEN";
  static String by = "BY";
  static String ct = "CT";
  static String dbaAndAka = "DBA & AKA";
  static String error = "ERROR";
  static String fOr = "FOR";
  static String frOm = "FROM";
  static String htta = "HTTA";
  static String iN = "IN";
  static String inPlace = "IN PLACE";
  static String inPlaceO = "IN PLACE O";
  static String init = "INIT";
  static String kna = "KNA";
  static String nka = "NKA";
  static String oF = "OF";
  static String pka = "PKA";
  static String subtitute = "SUBSTITUTE";
  static String the = "THE";
  static String through = "THROUGH";
  static String tO = "TO";
  static String toBe = "TO BE";
  static String toThe = "TO THE";
  static String under = "UNDER";
  static String underThe = "UNDER THE";
  static String _with = "WITH";
  static String buyer = "BUYER";
  static String divisionOf = "DIV";
  static String employer = "EMPLOY";
  static String issurerOf = "ISS";
  static String nowMergedBy = "MERG";
  static String nextOfFriend = "NOF";
  static String nextOfKin = "NOK";
  static String asNominee = "NOM";
  static String substitutedParty = "SUB";
  static String subrogee = "SUBROGEE";
  static String successorTo = "SUCC TO";
  static String trustee = "TR";
  static String victimRepresentative = "VICTIM_REP";

  static List<String> akaSelectList = [
    administratorOf,
    acting,
    estateOf,
    fkn,
    fna,
    orHis,
    suc,
    sucIn,
    dba,
    byMerger,
    company,
    fDBA,
    nbm,
    sbm,
    sbmt,
    tdba,
    aka,
    fka,
    slash,
    amendedTo,
    solely,
    among,
    an,
    andAs,
    andAsThe,
    andOr,
    as,
    between,
    by,
    ct,
    dbaAndAka,
    error,
    fOr,
    frOm,
    htta,
    iN,
    inPlace,
    inPlaceO,
    init,
    kna,
    nka,
    oF,
    pka,
    subtitute,
    the,
    through,
    tO,
    toBe,
    toThe,
    under,
    underThe,
    _with,
    buyer,
    divisionOf,
    employer,
    issurerOf,
    nowMergedBy,
    nextOfFriend,
    nextOfKin,
    asNominee,
    substitutedParty,
    subrogee,
    successorTo,
    trustee,
    victimRepresentative,
  ];
}