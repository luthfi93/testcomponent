import 'package:eno_dart_components_lib/efiling_core/models/domain.dart';
import 'package:exportable/exportable.dart';

class ClerkStatusSummary extends Domain {
  @export int newCourt = 0;
  @export int newCase = 0;
  @export int newFiling = 0;
}