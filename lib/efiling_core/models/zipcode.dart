import 'dart:convert';
import 'package:eno_dart_components_lib/efiling_core/models/domain.dart';
import 'package:exportable/exportable.dart';

class ZipCode extends Domain{
  @export String zipCode="";
  @export String latitude;
  @export String longitude;

  @export String state="";
  @export String city="";
  @export String county;
  @export String cityAlias="";

  convertFromJson(zipCodeJson){
    ZipCode zipCode = new ZipCode();
    zipCode.initFromJson(JSON.encode(zipCodeJson));

    return zipCode;
  }

  Map getSearchZipCode(){
    Map zipCodeMap = new Map();

    zipCodeMap['state'] = this.state;
    zipCodeMap['city'] = this.city;
    zipCodeMap['id'] = this.id;
    zipCodeMap['zipCode'] = this.zipCode;
    zipCodeMap['cityAlias'] = this.cityAlias;
    zipCodeMap['name'] = this.name;

    return zipCodeMap;
  }

}