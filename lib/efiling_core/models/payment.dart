import 'package:eno_dart_components_lib/efiling_core/models/domain.dart';
import 'package:exportable/exportable.dart';

class Payment extends Domain {
  @export int receiptId = 0;
  @export String cashbookCode = "";
  @export int receiptNumber = 0;
  @export int postsetId = 0;
  @export String comment = "";

  Payment convertFromJson(Map paymentMap){
    Payment payment = new Payment();
    payment.initFromMap(paymentMap);

    return payment;
  }
}