import 'package:eno_dart_components_lib/efiling_core/models/document.dart';
import 'package:eno_dart_components_lib/efiling_core/models/domain.dart';
import 'package:exportable/exportable.dart';

class DocumentList extends Domain {
  @export List<Document> docList = new List<Document>();
  dynamic docListJson;

  Map convertToMap(){
    Map documentListMap = this.toMap();

    //convert persons object list to map list
    List<Map> docListMap = new List<Map>();
    this.docList.forEach((Document document){
      docListMap.add(document.convertToMap());
    });
    documentListMap['docList'] = docListMap;

    return documentListMap;
  }

  DocumentList convertFromJson(docListJson){
    DocumentList docList = new DocumentList();
    docList.initFromMap(docListJson);

    List<Document> documentList = new List<Document>();
    for(dynamic docJson in docListJson['docList']){
      Document document = new Document();
      document = document.convertFromJson(docJson);
      documentList.add(document);
    }

    docList.docListJson = docListJson;
    docList.docList = documentList;
    return docList;
  }

}