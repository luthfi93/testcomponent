import 'package:eno_dart_components_lib/efiling_core/models/action_code.dart';
import 'package:eno_dart_components_lib/efiling_core/models/attorney.dart';
import 'package:eno_dart_components_lib/efiling_core/models/billing_transaction.dart';
import 'package:eno_dart_components_lib/efiling_core/models/case_number.dart';
import 'package:eno_dart_components_lib/efiling_core/models/case_result.dart';
import 'package:eno_dart_components_lib/efiling_core/models/docket_code.dart';
import 'package:eno_dart_components_lib/efiling_core/models/document.dart';
import 'package:eno_dart_components_lib/efiling_core/models/domain.dart';
import 'package:eno_dart_components_lib/efiling_core/models/party.dart';
import 'package:eno_dart_components_lib/efiling_core/models/payment.dart';
import 'package:eno_dart_components_lib/efiling_core/models/service_contact.dart';
import 'package:eno_dart_components_lib/efiling_core/models/simple_filer.dart';
import 'package:exportable/exportable.dart';

class Filing extends Domain {

  @export CaseNumber caseNumber = new CaseNumber();
  @export String pendingCaseNumber = "";
  @export List<Document> documents = new List<Document>();
  @export String filingId = "";
  @export String jobId = "";

  @export CaseResult caseResult = new CaseResult();
  @export DateTime filingDate;
  @export String filingDateUrl = "";

  @export Payment payment = new Payment();
  @export bool saveForLater = false;
  @export bool emergencyFlag = false;
  @export ActionCode actionCode = new ActionCode();
  @export DocketCode docketCode = new DocketCode();

  @export int portalUserId = 0;
  @export int docketId = 0;
  @export int creditCardId =0;

  @export BillingTransaction billingTransaction = new BillingTransaction();

  @export List<Party> plaintiffList = new List<Party>();
  @export List<Party> defendantList = new List<Party>();
  @export List<Party> proposedParties = new List<Party>();
  @export List<Party> appearanceParties = new List<Party>();

  @export String filingType = "";
  @export SimpleFiler filer = new SimpleFiler();
  @export String courtRoomCode = "";
  @export bool freeFiling = false;
  @export bool alreadyServedBySheriff = false;
  @export String reasonForFreeFiling = "";

  @export Attorney filingAttorney = new Attorney();
  @export String docketText = "";
  @export DateTime appointmentDate;

  @export List<ServiceContact> serviceContactList = new List<ServiceContact>();
  @export Party partyResponsibleForFilingFee = new Party();
  @export String officeCode = "";
  @export bool withJuryDemand = false;
  @export String caseType = "";


  Map convertToMap(){
    Map filingMap = this.toMap();

    //convert documents list to map list
    List<Document> documents = this.documents;
    List<dynamic> documentsMapList = new List<dynamic>();
    documents.forEach((Document document){
      documentsMapList.add(document.convertToMap());
    });
    filingMap['documents'] = documentsMapList;

    filingMap['caseResult'] = this.caseResult.convertToMap();

    //convert plaintiff party list to map list
    List<Party> plaintiffList = this.plaintiffList;
    List<dynamic> plaintiffMapList = new List<dynamic>();
    plaintiffList.forEach((Party party){
      plaintiffMapList.add(party.convertToMap());
    });
    filingMap['plaintiffList'] = plaintiffMapList;

    //convert defendant party list to map list
    List<Party> defendantList = this.defendantList;
    List<dynamic> defendantMapList = new List<dynamic>();
    defendantList.forEach((Party party){
      defendantMapList.add(party.convertToMap());
    });
    filingMap['defendantList'] = defendantMapList;

    filingMap['filingAttorney'] = this.filingAttorney.convertToMap();

    filingMap['billingTransaction'] = this.billingTransaction.convertToMap();

    List<dynamic> serviceContactMapList = new List<dynamic>();
    this.serviceContactList.forEach((ServiceContact serviceContact){
      serviceContactMapList.add(serviceContact.convertToMap());
    });
    filingMap['serviceContactList'] = serviceContactMapList;

    filingMap['partyResponsibleForFilingFee'] = this.partyResponsibleForFilingFee.convertToMap();

    return filingMap;

  }

  Filing convertFromJson(Map filingMap){
    Filing filing = new Filing();
    filing.initFromMap(filingMap);
    List<Document> documents = new List<Document>();

    for(dynamic documentMap in filingMap['documents']){
      Document document = new Document();
      document = document.convertFromJson(documentMap);
      documents.add(document);
    }
    filing.documents = documents;

    CaseResult caseResult = new CaseResult();
    filing.caseResult = caseResult.convertFromJson(filingMap['caseResult']);

    List<Party> plaintiffList = new List<Party>();
    for(dynamic plaintiffMap in filingMap['plaintiffList']){
      Party party = new Party();
      party = party.convertFromJson(plaintiffMap);
      plaintiffList.add(party);
    }
    filing.plaintiffList = plaintiffList;

    List<Party> defendantList = new List<Party>();
    for(dynamic plaintiffMap in filingMap['defendantList']){
      Party party = new Party();
      party = party.convertFromJson(plaintiffMap);
      defendantList.add(party);
    }
    filing.defendantList = defendantList;

    if(filingMap['filingAttorney'] != null){
      Attorney filingAttorney = new Attorney();
      filing.filingAttorney = filingAttorney.convertFromJson(filingMap['filingAttorney']);
    }

    List<ServiceContact> serviceContactList = new List<ServiceContact>();
    for(dynamic serviceContactMap in filingMap['serviceContactList']){
      ServiceContact serviceContact = new ServiceContact();
      serviceContact = serviceContact.convertFromJson(serviceContactMap);
      serviceContactList.add(serviceContact);
    }
    filing.serviceContactList = serviceContactList;


    if(filingMap['partyResponsibleForFilingFee'] != null){
      Party partyResponsibleForFilingFee = new Party();
      filing.partyResponsibleForFilingFee = partyResponsibleForFilingFee.convertFromJson(filingMap['partyResponsibleForFilingFee']);
    }

    if(filingMap['actionCode'] != null){
      filing.actionCode = actionCode.convertFromJson(filingMap['actionCode']);
    }

    if(filingMap['docketCode'] != null){
      filing.docketCode = docketCode.convertFromJson(filingMap['docketCode']);
    }

    if(filingMap['filer'] != null){
      filing.filer = filer.convertFromJson(filingMap['filer']);
    }

    if(filingMap['billingTransaction'] != null){
      filing.billingTransaction = billingTransaction.convertFromJson(filingMap['billingTransaction']);
    }

    return filing;
  }

}