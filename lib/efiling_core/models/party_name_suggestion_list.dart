import 'package:eno_dart_components_lib/efiling_core/models/domain.dart';
import 'package:eno_dart_components_lib/efiling_core/models/party_name_suggestion.dart';
import 'package:exportable/exportable.dart';

class PartyNameSuggestionList extends Domain {
  @export List<PartyNameSuggestion> partyNameSuggestionList = new List<PartyNameSuggestion>();

  PartyNameSuggestionList convertFromJson(partyNameSuggestionListJson){
    PartyNameSuggestionList PartyNameSuggestionListobj = new PartyNameSuggestionList();

    List<PartyNameSuggestion> partyNameSuggestionList = new List<PartyNameSuggestion>();
    for(dynamic partyNameSuggestionJson in partyNameSuggestionListJson['partyNameSuggestionList']){
      PartyNameSuggestion partyNameSuggestion = new PartyNameSuggestion();
      partyNameSuggestion = partyNameSuggestion.convertFromJson(partyNameSuggestionJson);
      partyNameSuggestionList.add(partyNameSuggestion);
    }

    PartyNameSuggestionListobj.partyNameSuggestionList = partyNameSuggestionList;
    return PartyNameSuggestionListobj;
  }

}