import 'package:eno_dart_components_lib/efiling_core/models/domain.dart';
import 'package:eno_dart_components_lib/efiling_core/models/queue.dart';
import 'package:exportable/exportable.dart';

class QueueList extends Domain {
  @export List<Queue> queueList = new List<Queue>();
  dynamic queueListJson;

  QueueList convertFromJson(queueListJson){
    QueueList queueListObj = new QueueList();
    queueListObj.initFromMap(queueListJson);

    List<Queue> queueList = new List<Queue>();
    for(dynamic queueJson in queueListJson['queueList']){
      Queue queue = new Queue();
      queue = queue.convertFromJson(queueJson);
      queueList.add(queue);
    }

    queueListObj.queueListJson = queueListJson;
    queueListObj.queueList = queueList;
    return queueListObj;
  }

}