import 'package:eno_dart_components_lib/efiling_core/models/domain.dart';
import 'package:exportable/exportable.dart';

class User extends Domain {

  static String FIRM_ADMIN_PORTAL_USER_GROUP = "LawFirmAdmin";
  static String STAFF_PORTAL_USER_GROUP = "LawFirmStaff";
  static String ATTORNEY_PORTAL_USER_GROUP = "Attorney";
  static String ADMIN_PORTAL_USER_GROUP = "Admin";
  static String INDIVIDUAL_PORTAL_USER_GROUP = "Individual";
  static List memberTypeList = [STAFF_PORTAL_USER_GROUP, FIRM_ADMIN_PORTAL_USER_GROUP, ATTORNEY_PORTAL_USER_GROUP];

  @export int _portalUserId = 0;
  @export int _defaultPortalUserId = 0;
  @export String login = "";
  @export String portalGroup = "";
  @export String principal = "";
  @export String authMethod = "";
  @export String password = "";
  @export String status = "";
  @export DateTime expiredAt = new DateTime.now();
  @export DateTime logoutDate = new DateTime.now();
  @export String requestorLogin = "";
  @export String trafficId = "";
  @export String sid = "";
  @export String caseFolderSid = "";
  @export String rjoSid = "";
  @export int calendarAccountId = 0;
  @export int externalId = 0;
  @export String barId = "";

  isIndividual(){
    return (portalGroup == INDIVIDUAL_PORTAL_USER_GROUP) ? true : false;
  }

  Map convertToMap(){
    Map userMap = this.toMap();
    return userMap;
  }

}