import 'package:eno_dart_components_lib/efiling_core/models/address.dart';
import 'package:eno_dart_components_lib/efiling_core/models/clerk_location.dart';
import 'package:eno_dart_components_lib/efiling_core/models/court_room.dart';
import 'package:eno_dart_components_lib/efiling_core/models/office.dart';
import 'package:exportable/exportable.dart';

class Court extends Office {
  @export ClerkLocation clerkLocation = new ClerkLocation();
  @export bool detailView = false;
  @export CourtRoom courtRoom = new CourtRoom();
  @export String courtClass = "";
  @export int defaultJudgeId =0;
  @export int defaultDrJudgeId = 0;
  @export String defaultJudgeIdValidation="";
  @export String calendarIds="";
  @export int defaultCalendarId=0;

  convertFromJSON(courtJSON){
    Court court = new Court();
    court.initFromMap(courtJSON);
    Address address = new Address();
    if(courtJSON['address'] != null) {
      address.initFromMap(courtJSON['address']);
    }
    court.address = address;

    ClerkLocation clerkLocation = new ClerkLocation();
    if(courtJSON['clerkLocation'] != null) {
      clerkLocation.initFromMap(courtJSON['clerkLocation']);
    }
    court.clerkLocation = clerkLocation;

    CourtRoom courtRoom = new CourtRoom();
    if(courtJSON['courtRoom'] != null) {
      courtRoom.initFromMap(courtJSON['courtRoom']);
    }
    court.courtRoom = courtRoom;
    return court;
  }
}