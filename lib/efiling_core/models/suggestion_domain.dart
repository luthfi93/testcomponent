import 'package:eno_dart_components_lib/efiling_core/models/domain.dart';
import 'package:exportable/exportable.dart';

class SuggestionDomain extends Domain {
  @export String suggestion = "";
  @export String className = "";
}