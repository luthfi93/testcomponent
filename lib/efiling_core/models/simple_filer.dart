import 'package:exportable/exportable.dart';

class SimpleFiler extends Object with Exportable{
  @export String name = "";
  @export String filerId = "";
  @export String type = "";

  SimpleFiler();

  String getFilerName(){
    return name;
  }

  Map convertToMap(){
    return this.toMap();
  }

  SimpleFiler convertFromJson(Map simpleFilerMap){
    SimpleFiler simpleFiler = new SimpleFiler();
    simpleFiler.initFromMap(simpleFilerMap);
    return simpleFiler;
  }

}