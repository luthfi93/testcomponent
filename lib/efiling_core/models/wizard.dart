import 'package:angular/angular.dart';
import 'package:eno_dart_components_lib/efiling_core/models/action_code.dart';
import 'package:eno_dart_components_lib/efiling_core/models/billing_info.dart';
import 'package:eno_dart_components_lib/efiling_core/models/county.dart';
import 'package:eno_dart_components_lib/efiling_core/models/court.dart';
import 'package:eno_dart_components_lib/efiling_core/models/court_staff.dart';
import 'package:eno_dart_components_lib/efiling_core/models/credit_card.dart';
import 'package:eno_dart_components_lib/efiling_core/models/docket_code.dart';
import 'package:eno_dart_components_lib/efiling_core/models/document.dart';
import 'package:eno_dart_components_lib/efiling_core/models/filing.dart';
import 'package:eno_dart_components_lib/efiling_core/models/party_code.dart';
import 'package:eno_dart_components_lib/efiling_core/models/party_name_suggestion.dart';
import 'package:eno_dart_components_lib/efiling_core/models/simple_filer.dart';
import 'package:eno_dart_components_lib/efiling_core/models/suffix_code.dart';
import 'package:exportable/exportable.dart';

enum WizardType {NewCase, NewFiling, AddFile, CourtWizard, PrintCapture}

@Injectable()
class Wizard extends Object with Exportable {
  @export List<SimpleFiler> filerList = new List<SimpleFiler>();
  @export Filing filing = new Filing();
  @export List<Document> reUseDocument = new List<Document>();
  @export bool resumedSavedForLater = false;

  @export CreditCard creditCardToUse = new CreditCard();
  @export String caseNumber = "";
  @export WizardType wizardType = WizardType.NewCase;

  @export int queuedId=-1;
  @export int queuedEventId=-1;

  @export String docViewerUrl;
  @export List<Document> printCaptureDocuments = new List<Document>();
  @export Set<Document> selectedPrintCaptureDocuments = new Set<Document>();
  @export String attorney = "";
  @export String ccInfo = "";
  @export String ccExpDate = "";
  @export String barId = "";
  @export bool countyDivisionSupported = false;
  @export Court court = new Court();
  @export String efilingImplementationVersion="3.0";
  @export String jobId = "";
  @export bool showInternationalAddressMode = false;
  @export bool showCountyDivRm4 = false;
  @export bool freeFiling = false;
  @export bool showCreateFormBtn = true;
  @export CourtStaff loggedCourtStaff = new CourtStaff();

  @export List<ActionCode> actionCodeList = new List<ActionCode>();
  @export List<DocketCode> docketCodeList = new List<DocketCode>();
  @export List<PartyCode> partyCodeList = new List<PartyCode>();
  @export List<SuffixCode> suffixCodeList = new List<SuffixCode>();

  @export BillingInfo billingInfo = new BillingInfo();

  @export String caseCategoryCode = "";
  @export String caseTypeCode = "";

  @export List<String> zipCodeSuggestions = new List<String>();
  @export List<PartyNameSuggestion> personNameSuggestions = new List<PartyNameSuggestion>();
  @export List<PartyNameSuggestion> companyNameSuggestions = new List<PartyNameSuggestion>();

  @export List<County> countyList = new List<County>();
  @export bool allowSubmit = false;

  Map convertToMap(){
    Map wizardMap = this.toMap();

    //convert filerList to map list
    List<Map> filerMapList = new List<Map>();
    this.filerList.forEach((SimpleFiler filer){
      filerMapList.add(filer.convertToMap());
    });
    wizardMap['filerList'] = filerMapList;

    //convert filing object to map
    wizardMap['filing'] = this.filing.convertToMap();
    wizardMap['creditCardToUse'] = this.creditCardToUse.convertToMap();

    //convert filerList to map list
    List<Map> printCaptureDocumentsMapList = new List<Map>();
    this.printCaptureDocuments.forEach((Document document){
      printCaptureDocumentsMapList.add(document.convertToMap());
    });
    wizardMap['printCaptureDocuments'] = printCaptureDocumentsMapList;

    //convert actionCodeList to map list
    List<Map> actionCodeMapList = new List<Map>();
    this.actionCodeList.forEach((ActionCode actionCode){
      actionCodeMapList.add(actionCode.convertToMap());
    });
    wizardMap['actionCodeList'] = actionCodeMapList;

    //convert docketCodeList to map list
    List<Map> docketCodeMapList = new List<Map>();
    this.docketCodeList.forEach((DocketCode docketCode){
      docketCodeMapList.add(docketCode.convertToMap());
    });
    wizardMap['docketCodeList'] = docketCodeMapList;

    //convert partyCodeList to map list
    List<Map> partyCodeMapList = new List<Map>();
    this.partyCodeList.forEach((PartyCode partyCode){
      partyCodeMapList.add(partyCode.convertToMap());
    });
    wizardMap['partyCodeList'] = partyCodeMapList;

    //convert partyCodeList to map list
    List<Map> suffixCodeMapList = new List<Map>();
    this.suffixCodeList.forEach((SuffixCode suffixCode){
      suffixCodeMapList.add(suffixCode.convertToMap());
    });
    wizardMap['suffixCodeList'] = suffixCodeMapList;

    wizardMap['billingInfo'] = this.billingInfo.convertToMap();

    //convert personNameSuggestions to map list
    List<Map> personNameSuggestionMapList = new List<Map>();
    this.personNameSuggestions.forEach((PartyNameSuggestion partyNameSuggestion){
      personNameSuggestionMapList.add(partyNameSuggestion.convertToMap());
    });
    wizardMap['personNameSuggestions'] = personNameSuggestionMapList;

    //convert companyNameSuggestions to map list
    List<Map> companyNameSuggestionMapList = new List<Map>();
    this.companyNameSuggestions.forEach((PartyNameSuggestion partyNameSuggestion){
      companyNameSuggestionMapList.add(partyNameSuggestion.convertToMap());
    });
    wizardMap['companyNameSuggestions'] = companyNameSuggestionMapList;

    //convert companyNameSuggestions to map list
    List<Map> countyListMap = new List<Map>();
    this.countyList.forEach((County county){
      countyListMap.add(county.convertToMap());
    });
    wizardMap['countyList'] = countyListMap;

    List<Map> reUseDocumentMapList = new List<Map>();
    this.reUseDocument.forEach((Document reUseDocument){
      filerMapList.add(reUseDocument.convertToMap());
    });
    wizardMap['reUseDocument'] = reUseDocumentMapList;

    return wizardMap;
  }

}