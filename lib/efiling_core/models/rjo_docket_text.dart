import 'package:eno_dart_components_lib/efiling_core/models/docket_text.dart';
import 'package:eno_dart_components_lib/efiling_core/models/document.dart';
import 'package:exportable/exportable.dart';

class RjoDocketText extends DocketText {
  @export List<Document> documents = new List<Document>();
  DateTime processed;

  Map convertToMap(){
    Map rjoDocketTextMap = this.toMap();

    //convert documents object list to map list
    List<Map> documentsMapList = new List<Map>();
    this.documents.forEach((Document document){
      documentsMapList.add(document.convertToMap());
    });
    rjoDocketTextMap['documents'] = documentsMapList;

    return rjoDocketTextMap;
  }
}