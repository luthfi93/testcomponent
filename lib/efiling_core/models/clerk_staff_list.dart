import 'dart:convert';
import 'package:exportable/exportable.dart';

import 'package:eno_dart_components_lib/efiling_core/models/clerk_staff.dart';
import 'package:eno_dart_components_lib/efiling_core/models/domain.dart';

class ClerkStaffList extends Domain{
  @export List<ClerkStaff> clerkStaffList = new List<ClerkStaff>();
  dynamic clerkStaffListJson;

  convertFromJson(ClerkStaffListJson){
    ClerkStaffList clerkStaffListObject = new ClerkStaffList();
    List<ClerkStaff> clerkStaffList = new List<ClerkStaff>();

    clerkStaffListObject.initFromJson(JSON.encode(ClerkStaffListJson));

    for(dynamic clerkStaffJson in ClerkStaffListJson['clerkStaffList']){
      ClerkStaff clerkStaff = new ClerkStaff();
      clerkStaff = clerkStaff.convertFromJson(clerkStaffJson);
      clerkStaffList.add(clerkStaff);
    }

    clerkStaffListObject.clerkStaffList = clerkStaffList;
    clerkStaffListObject.clerkStaffListJson = ClerkStaffListJson;
    return clerkStaffListObject;
  }

}