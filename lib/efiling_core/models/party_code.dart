import 'package:eno_dart_components_lib/efiling_core/models/domain.dart';
import 'package:exportable/exportable.dart';

class PartyCode extends Domain {
  @export String value = "";
  @export String label = "";

  Map convertToMap(){
    return this.toMap();
  }
}