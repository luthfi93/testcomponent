import 'package:eno_dart_components_lib/efiling_core/models/action_code.dart';
import 'package:eno_dart_components_lib/efiling_core/models/domain.dart';
import 'package:exportable/exportable.dart';

class ActionCodeList extends Domain {
  @export List<ActionCode> actionCodeList = new List<ActionCode>();

  ActionCodeList convertFromJson(Map actionCodeListJson){
    ActionCodeList actionCodeListObj = new ActionCodeList();
    actionCodeListObj.initFromMap(actionCodeListJson);

    List<ActionCode> actionCodeList = new List<ActionCode>();

    if(actionCodeListJson != null){
      if(actionCodeListJson['actionCodeList'] != null){
        for(Map actionCodeJson in actionCodeListJson['actionCodeList']){
          ActionCode actionCode = new ActionCode();
          actionCode = actionCode.convertFromJson(actionCodeJson);
          actionCodeList.add(actionCode);
        }
      }

    }

    actionCodeListObj.actionCodeList = actionCodeList;
    return actionCodeListObj;
  }

}