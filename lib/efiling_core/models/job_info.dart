import 'package:eno_dart_components_lib/efiling_core/models/domain.dart';
import 'package:exportable/exportable.dart';

class JobInfo extends Domain {

  static String STATUS_PROCESSING = "PROCESSING";
  static String STATUS_PROCESSED = "PROCESSED";
  static String STATUS_ERROR = "ERROR";

  @export String jobId = "";
  @export String caseNumber = "";
  @export String status = "";
  @export int userId = 0;
  @export int filingId = 0;
  @export String error = "";
  @export String actionStatusCorrelationId = "";
  @export String jobType = "";
  @export int queuedEventId = 0;
  @export String userIdStatus = "";
  @export String caseCourt = "";
  @export String envId = "";
  @export String documentReceivedDate = "";
  @export String queryName = "";
  @export String queryString = "";
  @export String description = "";

  Map convertToMap(){
    return this.toMap();
  }

  JobInfo convertFromJson(Map jobInfoJson){
    JobInfo jobInfo = new JobInfo();
    jobInfo.initFromMap(jobInfoJson);

    return jobInfo;
  }
}