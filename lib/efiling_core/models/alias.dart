import 'package:eno_dart_components_lib/efiling_core/models/domain.dart';
import 'package:exportable/exportable.dart';

class Alias extends Domain {
  @export int partyId = 0;
  @export String lastName = "";
  @export String firstName = "";
  @export String middleInitial = "";
  @export String nameSuffix = "";
  @export String nameSuffixCode = "";
  @export String namePrefix = "";
  @export String companyName = "";
  @export String aliasType = "";
  @export bool asCompany = false;

  Map convertToMap(){
    return this.toMap();
  }

  Alias convertFromJson(Map aliasMap){
    Alias alias = new Alias();
    alias.initFromMap(aliasMap);
    return alias;
  }
}