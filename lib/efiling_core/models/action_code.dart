import 'package:exportable/exportable.dart';
import 'package:eno_dart_components_lib/efiling_core/models/domain.dart';

class ActionCode extends Domain {
  @export String code = "";
  @export String desc = "";
  @export double amount = 0.0;
  @export List<String> caseTypes = new List<String>();

  ActionCode convertFromJson(Map actionCodeJson){
    ActionCode actionCode = new ActionCode();
    actionCode.initFromMap(actionCodeJson);

    List<String> caseTypes = new List<String>();

    if(actionCodeJson != null){
      if(actionCodeJson['caseTypes'] != null){
        for(String caseType in actionCodeJson['caseTypes']){
          caseTypes.add(caseType);
        }
      }
    }

    actionCode.caseTypes = caseTypes;
    return actionCode;
  }

  Map convertToMap(){
    Map actionCodeMap = this.toMap();
    return actionCodeMap;
  }

}