import 'package:eno_dart_components_lib/efiling_core/models/domain.dart';
import 'package:eno_dart_components_lib/efiling_core/models/service_method.dart';
import 'package:exportable/exportable.dart';

class Services extends Domain {
  @export List<ServiceMethod> serviceMethodList = new List<ServiceMethod>();
  @export String services = "";

  Map convertToMap(){
    Map servicesMap = this.toMap();

    List<ServiceMethod> serviceMethodList = this.serviceMethodList;

    List<dynamic> serviceMethodMapList = new List<dynamic>();
    serviceMethodList.forEach((ServiceMethod serviceMethod){
      serviceMethodMapList.add(serviceMethod.convertToMap());
    });

    servicesMap['serviceMethodList'] = serviceMethodMapList;

    return servicesMap;
  }
}