import 'package:eno_dart_components_lib/efiling_core/models/person.dart';
import 'package:exportable/exportable.dart';

class Officer extends Person {
  @export int officerId = 0;
  @export String badgeNumber = "";
  @export String jurisdictionCode = "";
  @export DateTime activeDate;
  @export DateTime deactivateDate;
  @export bool partyFlag = false;
  @export String officeAgencyCode = "";

  Map convertToMap(){
    return this.toMap();
  }
}