import 'package:exportable/exportable.dart';
import 'package:eno_dart_components_lib/efiling_core/models/base_efsp_object.dart';

class RegistrantInfo extends BaseEfspObj{
  @export String efspProviderName;
  @export String registerEmail;
  @export int memberId;
  @export String firstName;
  @export String lastName;
  @export String middleName;
  @export int officeId;
  @export String officeName;
  @export int userId;
  @export String attorneyId;
  @export String role;
  @export String passwordResetQuestion;
  @export String passwordResetAnswer;

}