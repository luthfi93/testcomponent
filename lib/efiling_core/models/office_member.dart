import 'dart:convert';
import 'package:eno_dart_components_lib/efiling_core/models/address.dart';
import 'package:eno_dart_components_lib/efiling_core/models/county_list.dart';
import 'package:eno_dart_components_lib/efiling_core/models/credit_card.dart';
import 'package:eno_dart_components_lib/efiling_core/models/notification_preferences.dart';
import 'package:eno_dart_components_lib/efiling_core/models/person.dart';
import 'package:eno_dart_components_lib/efiling_core/models/queue.dart';
import 'package:exportable/exportable.dart';

class OfficeMember extends Person {

  @export String memberType = "";
  @export String eServiceEmails = "";
  @export String reminderEmails = "";
  @export String fax = "";
  @export Address address = new Address();
  @export bool detailView = false;
  @export String officeName = "";
  @export int officeId = 0;
  @export String notes = "";
  @export String approvedLogin = "";
  @export int userId = 0;
  @export Queue queue = new Queue();
  @export String barId = "";
  @export int attorneyId = 0;
  @export String subscriptionToDate = "";
  @export String subscriptionFromDate = "";
  @export bool allowFreeFiling = false;
  @export String reasonForFreeFiling = "";
  @export bool showFreeFilingFeature ;
  @export bool triage = false;
  @export bool supervisor = false;
  @export bool judge = false;
  @export bool judgeProTem = false;
  @export bool seniorJudge = false;
  @export bool temporaryJudge = false;
  @export bool magistrate = false;
  @export bool courtStaff = false;
  @export bool commissioner = false;
  @export bool facilitator = false;
  @export bool referee = false;
  @export String courtStaffType = "";
  @export String courtStaffTypeListBox = "";
  @export bool clerkDeputy = false;
  @export bool clerkSupervisor = false;
  @export bool casePool = false;
  @export bool courtPool = false;
  @export bool filingPool = false;
  @export String courtViewID = "";
  @export int judgeId = 0;
  String judgeIdStr = "";
  @export String judgeIdLabel = "";
  @export CreditCard creditCard = new CreditCard();
  @export bool activeQueue = false;
  @export bool allowedToFileOnBehalfOfAttorney = false;
  @export String passwordResetQuestion = "";
  @export String passwordResetAnswer = "";
  @export NotificationPreferences notificationPreferences = new NotificationPreferences();
  @export CountyList countyList = new CountyList();

  String password = "";
  String confirmPassword = "";
  dynamic officeMemberJson;

  String getDisplayName(){

    String displayName = "";

    if (this.namePrefix != null && (this.namePrefix != "")){
      displayName += this.namePrefix  + " ";
    }

    displayName += firstName;

    if (this.middleInitial != null && this.middleInitial != ""){
      displayName += " " + this.middleInitial ;
    }
    if (this.lastName!= null && this.lastName != ""){
      displayName += " " + this.lastName;
    }

    if (this.namePrefix!= null && this.namePrefix != ""){
      displayName += " " + this.namePrefix;
    }

    return displayName;
  }

  Map convertToMap(){
    Map map = this.toMap();
    map['officeMember']['address'] = this.address.convertToMap();
    return map;
  }

  convertFromJson(officeMemberJson){
    OfficeMember officeMemberObj = new OfficeMember();
    Address address = new Address();
    CreditCard creditCard = new CreditCard();

    officeMemberObj.initFromJson(JSON.encode(officeMemberJson));
    address.initFromJson(JSON.encode(officeMemberJson['address']));
    creditCard.initFromJson(JSON.encode(officeMemberJson['creditCard']));

    officeMemberObj.address = address;
    officeMemberObj.creditCard = creditCard;
    officeMemberObj.officeMemberJson = officeMemberJson;

    return officeMemberObj;
  }

}