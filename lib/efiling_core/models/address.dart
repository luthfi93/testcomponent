import 'package:eno_dart_components_lib/efiling_core/models/domain.dart';
import 'package:eno_dart_components_lib/efiling_core/models/service_method.dart';
import 'package:exportable/exportable.dart';

class Address extends Domain {
  @export String seq = "";
  @export String line1 = "";
  @export String line2 = "";
  @export String line3 = "";
  @export String city = "";
  @export String state = "";
  String stateLabel = "";
  @export String zip = "";
  @export String country = "";
  @export List<ServiceMethod> serviceMethods = new List<ServiceMethod>();
  @export bool primaryAddress = false;
  @export bool unknownAddress = false;

  String strCityStateZip = "";

  Map convertToMap(){
    Map addressMap = this.toMap();

    //convert service method object list to map list
    List<ServiceMethod> serviceMethodList = this.serviceMethods;
    List<Map> serviceMethodsMap = new List<Map>();
    serviceMethodList.forEach((ServiceMethod serviceMethod){
      serviceMethodsMap.add(serviceMethod.convertToMap());
    });
    addressMap['serviceMethods'] = serviceMethodsMap;

    return addressMap;
  }

  Address convertFromJson(Map addressMap){
    Address address = new Address();
    address.initFromMap(addressMap);

    List<ServiceMethod> serviceMethods = new List<ServiceMethod>();

    if(addressMap != null){
      if(addressMap['serviceMethods']){
        for(dynamic serviceMethodMap in addressMap['serviceMethods']){
          ServiceMethod serviceMethod = new ServiceMethod();
          serviceMethod = serviceMethod.convertFromJson(serviceMethodMap);
          serviceMethods.add(serviceMethod);
        }
      }
    }

    address.serviceMethods = serviceMethods;
    return address;
  }

}