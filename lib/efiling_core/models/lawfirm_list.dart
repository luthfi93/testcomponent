import 'dart:convert';
import 'package:exportable/exportable.dart';
import 'package:eno_dart_components_lib/efiling_core/models/domain.dart';
import 'package:eno_dart_components_lib/efiling_core/models/lawfirm.dart';

class LawFirmList extends Domain {

  @export List<LawFirm> lawFirmList = new List<LawFirm>();

  LawFirmList convertFromJson(lawFirmListJson){
    LawFirmList lawFirmListObject = new LawFirmList();
    lawFirmListObject.initFromJson(JSON.encode(lawFirmListJson));

    List<LawFirm> lawFirmList = new List<LawFirm>();

    for(dynamic lawFirmJson in lawFirmListJson['lawFirmList']){
      LawFirm lawFirm = new LawFirm();
      lawFirm = lawFirm.convertFromJson(lawFirmJson);
      lawFirmList.add(lawFirm);
    }

    lawFirmListObject.lawFirmList = lawFirmList;

    return lawFirmListObject;
  }

}