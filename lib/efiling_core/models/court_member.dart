import 'package:eno_dart_components_lib/efiling_core/models/calendar_info.dart';
import 'package:eno_dart_components_lib/efiling_core/models/court.dart';
import 'package:eno_dart_components_lib/efiling_core/models/office_member.dart';
import 'package:eno_dart_components_lib/efiling_core/models/subscription.dart';
import 'package:exportable/exportable.dart';

class CourtMember extends OfficeMember {

  @export Court court = new Court();
  @export Subscription subscription;
  @export CalendarInfo calendarInfo;

}