import 'package:eno_dart_components_lib/efiling_core/models/address.dart';
import 'package:eno_dart_components_lib/efiling_core/models/suggestion_domain.dart';
import 'package:exportable/exportable.dart';

class PartyNameSuggestion extends SuggestionDomain {
  @export int lawFirmId = 0;
  @export Address address = new Address();
  @export String firstName = "";
  @export String lastName = "";
  @export String companyName = "";
  @export bool asCompany = false;

  PartyNameSuggestion convertFromJson(partyNameSuggestionJson){
    PartyNameSuggestion partyNameSuggestion = new PartyNameSuggestion();
    partyNameSuggestion.initFromMap(partyNameSuggestionJson);

    Address address = new Address();
    address.initFromMap(partyNameSuggestionJson['address']);
    partyNameSuggestion.address = address;
    return partyNameSuggestion;
  }

  Map convertToMap(){
    Map partyNameSuggestionMap = this.toMap();

    //convert address object to map
    partyNameSuggestionMap['address'] = this.address.convertToMap();

    return partyNameSuggestionMap;
  }

}