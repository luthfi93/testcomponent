import 'package:eno_dart_components_lib/efiling_core/models/domain.dart';
import 'package:eno_dart_components_lib/efiling_core/models/zipcode.dart';


class ZipCodeList extends Domain {

  List<ZipCode> zipCodeList = new List<ZipCode>();

  convertFromJson(zipCodeJsonList){

    ZipCodeList zipCodeListObj = new ZipCodeList();
    List<ZipCode> zipCodeList = new List<ZipCode>();

    for(dynamic zipCodeJson in zipCodeJsonList['zipCodeList']){
      ZipCode zipCode = new ZipCode();
      zipCode = zipCode.convertFromJson(zipCodeJson);
      zipCodeList.add(zipCode);
    }

    zipCodeListObj.zipCodeList = zipCodeList;
    return zipCodeListObj;
  }

}

