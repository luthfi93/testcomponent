import 'dart:convert';
import 'package:eno_dart_components_lib/efiling_core/models/domain.dart';
import 'package:eno_dart_components_lib/efiling_core/models/user.dart';
import 'package:exportable/exportable.dart';

class Queue extends Domain {
  @export User user;
  @export String queueType;
  @export String queueName;
  @export String courtId;
  @export int clerkLocationId;
  @export int clerkStaffId;
  @export int size;
  dynamic queueJson;

  static final String USER_QUEUE = "UserQueue";
  static final String ESERVICE_QUEUE = "EserviceQueue";
  static final String WORKING_QUEUE = "WorkingQueue";
  static final String CLERK_SUPERVISOR_QUEUE = "ClerkSupervisorQueue";
  static final String COURT_QUEUE = "CourtQueue";
  static final String COURT_WORKING_QUEUE = "CourtWorkingQueue";

  static final List<String> viewMode = ["Queue", "Archived", "EService"];

  convertFromJson(queueJson){
    Queue queue = new Queue();
    queue.initFromJson(JSON.encode(queueJson));

    if(queueJson['user'] != null){
      User user = new User();
      user.initFromJson(JSON.encode(queueJson['user']));
      queue.user = user;
    }

    queue.queueJson = queueJson;
    return queue;
  }

  Map convertToMap(){
    Map queueMap = this.toMap();
    if(this.user != null){
      queueMap['user'] = this.user.convertToMap();
    }
    return queueMap;
  }

}
