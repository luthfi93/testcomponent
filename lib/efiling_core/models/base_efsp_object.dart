import 'package:exportable/exportable.dart';

class BaseEfspObj extends Object with Exportable{
  @export String key;
  @export String createdDateStr;
}