import 'package:eno_dart_components_lib/efiling_core/models/domain.dart';
import 'package:eno_dart_components_lib/efiling_core/models/party.dart';
import 'package:exportable/exportable.dart';

class PartyList extends Domain {

  @export List<Party> partyList = new List<Party>();

  PartyList convertFromJson(partyJsonList){

    PartyList partyListObject = new PartyList();
    partyListObject.initFromMap(partyJsonList);
    List<Party> partyList = new List<Party>();

    for(dynamic partyJSON in partyJsonList['partyList']){
      Party party = new Party();
      party = party.convertFromJSON(partyJSON);
      partyList.add(party);
    }

    partyListObject.partyList = partyList;
    return partyListObject;
  }

}
