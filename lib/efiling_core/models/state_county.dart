import 'package:exportable/exportable.dart';
import 'package:eno_dart_components_lib/efiling_core/models/domain.dart';

class StateCounty extends Domain {
  @export int stateId = 0;
  @export String countyCode = "";
  bool selected = false;
}