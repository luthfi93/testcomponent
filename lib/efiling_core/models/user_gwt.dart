import 'package:eno_dart_components_lib/efiling_core/models/office.dart';
import 'package:eno_dart_components_lib/efiling_core/models/user_module.dart';
import 'package:exportable/exportable.dart';

class UserGWT extends Object with Exportable{

  @export String login = "";
  @export String group = "";
  @export String name = "";
  @export String accessToken = "";

  //security access token to authorize access to pdf link
  @export int queueId = 0;
  @export String portalGroup = "";

  //subsciption
  @export bool validSubscription;
  @export String sid = "";

  @export String barId = "";
  @export int userId = 0;
  @export String caseFolderUrl = "";
  @export String addCalendarUrl = "";
  @export String courtCalendarUrl = "";

  @export String county = "";
  @export Office office = new Office();

  UserGWT createUserGWTFromUserModule(UserModule userModule){
    UserGWT userGWT = null;
    if(userModule!=null){
      userGWT = new UserGWT();
      userGWT.login= userModule.login;
      userGWT.name = userModule.name;
      if(userModule.office!=null){
        userGWT.group = userModule.office.name;
      }
      userGWT.queueId = userModule.defaultQueueId;
      userGWT.sid = userModule.authToken;

      String barId = userModule.user.barId;
      userGWT.barId = barId;
      userGWT.userId = userModule.user.id;
    }
    return userGWT;

  }

}
