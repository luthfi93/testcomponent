import 'package:eno_dart_components_lib/efiling_core/models/domain.dart';
import 'package:exportable/exportable.dart';

class BaseValueLabel extends Domain {
  @export String value;
  @export String label;

}