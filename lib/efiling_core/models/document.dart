import 'package:eno_dart_components_lib/efiling_core/models/docket_code.dart';
import 'package:eno_dart_components_lib/efiling_core/models/domain.dart';
import 'package:eno_dart_components_lib/efiling_core/models/file_url.dart';
import 'package:exportable/exportable.dart';

class Document extends Domain {
  @export String description = "";
  @export String lcDocId = "";
  @export String originalLcDocId = "";
  @export String newCaseOriginalLcDocId = "";
  @export String docViewerURL = "";
  @export String attorney = "";
  @export String party = "";
  @export String filedDate = "";
  @export String processedDate = "";
  @export String signedBy = "";
  @export String docViewerPdfUrl = "";
  @export int verification = 0;
  @export String defectiveReason = "";
  @export String changeReason = "";
  @export String filingId = "";
  @export String fileUrlString = "";
  @export bool defective = false;
  @export String fileName = "";
  @export String filePath = "";
  @export String uploadFilePath = "";
  @export String uploadDocViewerUrl = "";
  @export bool proposed = false;
  @export bool generated = false;
  @export int numberOfCopy = 0;
  @export int privacy = 0;
  @export bool viewAuthorized = false;
  @export bool enableCaptureDoc = false;
  @export bool embeddedObjectCheckRequired = false;
  @export bool excludedFromDocket = false;
  @export bool excludedFromFiling = false;
  @export bool judgeStampNeeded = false;
  @export bool signed = false;
  @export String quickFormHtml = "";
  @export DocketCode docketCode = new DocketCode();
  @export int pageNumberForSignature = 0;
  @export bool esigned = false;
  @export bool handSigned = false;
  @export String filingDescription = "";
  @export FileUrl fileUrl = new FileUrl();

  bool isPdf = false;
  bool isSelected = false;

  Map convertToMap(){
    Map documentMap = this.toMap();
    documentMap["docketCode"] = this.docketCode.convertToMap();
    documentMap["fileUrl"] = this.fileUrl.convertToMap();
    return documentMap;
  }

  Document convertFromJson(Map documentMap){
    Document document = new Document();
    document.initFromMap(documentMap);
    if(documentMap['docketCode'] != null) {
      DocketCode docketCode = new DocketCode();
      document.docketCode = docketCode.convertFromJson(documentMap['docketCode']);
    }
    if(documentMap['fileUrl'] != null){
      FileUrl fileUrl = new FileUrl();
      document.fileUrl = fileUrl.convertFromJson(documentMap['fileUrl']);
    }
    return document;
  }
}