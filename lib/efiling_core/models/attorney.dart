import 'package:eno_dart_components_lib/efiling_core/models/address.dart';
import 'package:eno_dart_components_lib/efiling_core/models/credit_card.dart';
import 'package:eno_dart_components_lib/efiling_core/models/office.dart';
import 'package:eno_dart_components_lib/efiling_core/models/person.dart';
import 'package:exportable/exportable.dart';

class Attorney extends Person {
  @export String seq = "";
  @export String barId = "";
  @export int attorneyId = 0;
  @export CreditCard creditCard = new CreditCard();
  @export Office office = new Office();
  @export Address address = new Address();
  @export String faxNumber = "";
  @export String attorneyCode = "";

  String getDisplayName(){

    String displayName = "";

    if (this.namePrefix != ""){
      displayName += this.namePrefix + " ";
    }

    displayName += this.firstName;

    if (this.middleInitial != ""){
      displayName += " " + this.middleInitial;
    }
    if (this.lastName != ""){
      displayName += " " + this.lastName;
    }

    if (this.nameSuffix != ""){
      displayName += " " + this.nameSuffix;
    }

    return displayName;
  }

  Map convertToMap(){
    Map attorneyMap = this.toMap();

    attorneyMap['creditCard'] = this.creditCard.convertToMap();
    attorneyMap['address'] = this.address.convertToMap();

    return attorneyMap;
  }

  Attorney convertFromJson(Map attorneyMap){
    Attorney attorney = new Attorney();
    attorney.initFromMap(attorneyMap);
    if(attorneyMap['creditCard'] != null){
      CreditCard creditCard = new CreditCard();
      attorney.creditCard = creditCard.convertFromJson(attorneyMap['creditCard']);
    }
    if(attorneyMap['office'] != null){
      Office office = new Office();
      attorney.office = office.convertFromJson(attorneyMap['office']);
    }
    if(attorneyMap['address'] != null){
      Address address = new Address();
      attorney.address = address.convertFromJson(attorneyMap['address']);
    }
    return attorney;
  }
}