import 'dart:convert';
import 'package:exportable/exportable.dart';
import 'package:eno_dart_components_lib/efiling_core/models/court_staff.dart';
import 'package:eno_dart_components_lib/efiling_core/models/domain.dart';

class CourtStaffList extends Domain {
  @export List<CourtStaff> courtStaffList = new List<CourtStaff>();

  convertFromJson(courtStaffJsonList){

    CourtStaffList courtStaffObjList = new CourtStaffList();
    courtStaffObjList.initFromJson(JSON.encode(courtStaffJsonList));

    List<CourtStaff> courtStaffList = new List<CourtStaff>();

    for(dynamic courtStaffJson in courtStaffJsonList['courtStaffList']){
      CourtStaff courtStaff = new CourtStaff();
      courtStaff = courtStaff.convertFromJson(courtStaffJson);
      courtStaffList.add(courtStaff);
    }

    courtStaffObjList.courtStaffList = courtStaffList;
    return courtStaffObjList;
  }
}
