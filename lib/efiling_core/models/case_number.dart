import 'package:eno_dart_components_lib/efiling_core/models/domain.dart';
import 'package:exportable/exportable.dart';

class CaseNumber extends Domain {

  @export String firstSegement = "";
  @export String courtAbbr = "";
  @export String caseType = "";
  @export String seqNumber = "";
  @export String yearMoth = "";
  @export String seqNumberNoFrontZeros = "";
  @export String caseNumber = "";

  CaseNumber convertFromJson(Map caseNumberMap){
    CaseNumber caseNumber = new CaseNumber();
    caseNumber.initFromMap(caseNumberMap);

    return caseNumber;
  }

}