import 'package:exportable/exportable.dart';

class AttorneyFiler extends Object with Exportable{
  @export String name = "";
  @export String barId = "";
  @export int attorneyId = 0;

  AttorneyFiler convertFromJson(Map attorneyFilerMap){
    AttorneyFiler attorneyFiler = new AttorneyFiler();
    attorneyFiler.initFromMap(attorneyFilerMap);
    return attorneyFiler;
  }

}
