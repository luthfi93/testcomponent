import 'package:eno_dart_components_lib/efiling_core/models/address.dart';
import 'package:eno_dart_components_lib/efiling_core/models/domain.dart';
import 'package:eno_dart_components_lib/efiling_core/models/office_member.dart';
import 'package:exportable/exportable.dart';

class Office extends Domain {
  @export Address address = new Address();
  @export String phone = "";
  @export String fax = "";
  @export String officeName = "";
  @export String regCode = "";
  @export bool regApproved = false;
  @export String officeEmail ="";
  @export String officeAbbr = "";
  @export List<OfficeMember> officeMembers = new List<OfficeMember>();

  convertFromJson(Map officeJson){
    Office office = new Office();
    office.initFromMap(officeJson);

    List<OfficeMember> officeMembers = new List<OfficeMember>();
    for(dynamic officeMemberJson in officeJson['officeMembers']){
      OfficeMember officeMember = new OfficeMember();
      officeMember = officeMember.convertFromJson(officeMemberJson);
      officeMembers.add(officeMember);
    }

    if(officeJson != null){
      if(officeJson["address"]){
        Address address = new Address();
        address.initFromMap(officeJson['address']);
        office.address = address;
      }
    }

    office.officeMembers = officeMembers;
    return office;
  }

}