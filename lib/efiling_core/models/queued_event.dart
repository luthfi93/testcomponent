import 'package:eno_dart_components_lib/efiling_core/models/case_description.dart';
import 'package:eno_dart_components_lib/efiling_core/models/case_type.dart';
import 'package:eno_dart_components_lib/efiling_core/models/domain.dart';
import 'package:eno_dart_components_lib/efiling_core/models/event.dart';
import 'package:eno_dart_components_lib/efiling_core/models/queue.dart';
import 'package:exportable/exportable.dart';

class QueuedEvent  extends Domain {
  @export Event event = new Event();
  @export bool active = true;
  @export bool archive;
  @export DateTime archiveDate;
  @export bool deleted;
  @export DateTime deletedDate;
  @export String actionBy;
  @export Queue routeToQueue = new Queue();
  @export String routeResultNote;
  @export String routeToCourtResultNote;
  @export bool markAsDone = true;
  @export CaseType caseType;
  @export CaseDescription caseDescription = new CaseDescription();


  QueuedEvent convertFromJson(Map queuedEventJson){
    QueuedEvent queuedEvent = new QueuedEvent();
    queuedEvent.initFromMap(queuedEventJson);

    Event event = new Event();
    if(queuedEventJson['event'] != null){
      queuedEvent.event = event.convertFromJson(queuedEventJson['event']);
    }

    return queuedEvent;
  }

}