import 'package:eno_dart_components_lib/efiling_core/models/domain.dart';
import 'package:eno_dart_components_lib/efiling_core/models/queued_event.dart';
import 'package:exportable/exportable.dart';

class QueuedEventList extends Domain {

  @export List<QueuedEvent> queuedEventList = new List<QueuedEvent>();

  QueuedEventList convertFromJson(Map queuedEventListJson){
    QueuedEventList queuedEventListObj = new QueuedEventList();
    queuedEventListObj.initFromMap(queuedEventListJson);

    List<QueuedEvent> queuedEventList = new List<QueuedEvent>();

    for(dynamic queuedEventJson in queuedEventListJson['queuedEventList']){
      if(queuedEventJson != null){
        QueuedEvent queuedEvent = new QueuedEvent();
        queuedEvent = queuedEvent.convertFromJson(queuedEventJson);
        queuedEventList.add(queuedEvent);
      }
    }

    queuedEventListObj.queuedEventList = queuedEventList;
    return queuedEventListObj;
  }
}
