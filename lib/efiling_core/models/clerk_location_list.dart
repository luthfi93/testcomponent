import 'dart:convert';
import 'package:exportable/exportable.dart';

import 'package:eno_dart_components_lib/efiling_core/models/domain.dart';
import 'package:eno_dart_components_lib/efiling_core/models/clerk_location.dart';

class ClerkLocationList extends Domain {
  @export  List<ClerkLocation> clerkLocationList = new List<ClerkLocation>();

  convertFromJson(clerkLocationJsonList){

    ClerkLocationList clerkLocationListObject = new ClerkLocationList();
    List<ClerkLocation> clerkLocationList = new List<ClerkLocation>();

    for(dynamic clerkLocationJson in clerkLocationJsonList){
      ClerkLocation clerkLocation = new ClerkLocation();
      clerkLocation = clerkLocation.convertFromJson(clerkLocationJson);
      clerkLocationList.add(clerkLocation);
    }

    clerkLocationListObject.clerkLocationList = clerkLocationList;
    return clerkLocationListObject;
  }

}