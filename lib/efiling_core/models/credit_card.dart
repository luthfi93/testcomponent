import 'package:eno_dart_components_lib/efiling_core/models/address.dart';
import 'package:eno_dart_components_lib/efiling_core/models/domain.dart';
import 'package:exportable/exportable.dart';

class CreditCard extends Domain {
  @export String number = "";
  @export String cciv = "";
  @export String expDate =  "";
  @export String expMonth ="";
  @export String expYear = "";
  @export String numberDisplay = "";
  @export String issuedTo = "";
  @export String cardType = "";
  @export bool accountDisabled = false;
  @export DateTime accountDisabledDate;
  @export Address address = new Address();
  @export String companyName = "";
  @export String phoneNumber = "";
  @export String faxNumber = "";
  @export String email = "";
  bool isExpired = false;
  String label = "";

  String addNumber = "";

  dynamic creditCardJson;

  static final creditCardType = ["VISA", "MASTER", "DISCOVER", "AMEX"];

  bool isWavier(){
    if(cardType != null && cardType != "" && cardType == "WV"){
      return true;
    }
    return false;
  }

  CreditCard convertFromJson(Map ccJson){
    CreditCard cc = new CreditCard();
    cc.initFromMap(ccJson);

    Address address = new Address();
    if(ccJson != null){
      if(ccJson['address'] != null){
        address = address.convertFromJson(ccJson['address']);
      }
    }
    cc.address = address;
    cc.creditCardJson = ccJson;
    return cc;
  }

  Map convertToMap(){
    Map creditCardMap = this.toMap();
    creditCardMap['address'] = this.address.convertToMap();
    return creditCardMap;
  }

}