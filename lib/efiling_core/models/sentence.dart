import 'package:eno_dart_components_lib/efiling_core/models/domain.dart';
import 'package:exportable/exportable.dart';

class Sentence extends Domain {
  @export int caseId = 0;
  @export int sequenceId = 0;
  @export int chargeNumber = 0;
  @export DateTime jailStartDate;
  @export int jailYears = 0;
  @export int jailMonths = 0;
  @export int jailDays = 0;

  Map convertToMap(){
    return this.toMap();
  }

  Sentence convertFromJson(sentenceJson){
    Sentence sentence = new Sentence();
    sentence.initFromMap(sentenceJson);
    return sentence;
  }
}