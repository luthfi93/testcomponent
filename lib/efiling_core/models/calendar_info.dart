import 'package:exportable/exportable.dart';
import 'package:eno_dart_components_lib/efiling_core/models/domain.dart';

class CalendarInfo extends Domain {
  @export String calendarUrl;
  @export String userName;
  @export String password;
}