import 'package:eno_dart_components_lib/efiling_core/models/domain.dart';
import 'package:exportable/exportable.dart';

class DocketCode extends Domain {
  @export String code = "";
  @export String desc = "";
  @export String actionCode = "";
  @export String docketApplication = "";
  @export double amount = 0.0;
  @export String eachAll = "";
  @export int numberOfPages = 0;
  @export bool chargedPerPage = false;
  @export List<String> acceptableCaseTypes = new List<String>();

  Map convertToMap(){
    return this.toMap();
  }

  DocketCode convertFromJson(Map docketCodeJson){
    DocketCode docketCode = new DocketCode();
    docketCode.initFromMap(docketCodeJson);
    return docketCode;
  }

}