import 'package:eno_dart_components_lib/efiling_core/models/domain.dart';
import 'package:eno_dart_components_lib/efiling_core/models/zipcode_suggetion.dart';
import 'package:exportable/exportable.dart';

class ZipCodeSuggestionList extends Domain {
  @export List<ZipCodeSuggestion> zipCodeSuggestionList = new List<ZipCodeSuggestion>();

  ZipCodeSuggestionList convertFromJson(zipCodeSuggestionJson){
    ZipCodeSuggestionList zipCodeSuggestionListObj = new ZipCodeSuggestionList();
    zipCodeSuggestionListObj.initFromMap(zipCodeSuggestionJson);

    List<ZipCodeSuggestion> zipCodeSuggestionList = new List<ZipCodeSuggestion>();
    for(dynamic actionCodeJson in zipCodeSuggestionJson['zipCodeSuggestionList']){
      ZipCodeSuggestion zipCodeSuggestion = new ZipCodeSuggestion();
      zipCodeSuggestion.initFromMap(actionCodeJson);
      zipCodeSuggestionList.add(zipCodeSuggestion);
    }

    zipCodeSuggestionListObj.zipCodeSuggestionList = zipCodeSuggestionList;
    return zipCodeSuggestionListObj;
  }
}