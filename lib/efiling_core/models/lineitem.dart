import 'package:exportable/exportable.dart';

class LineItem extends Object with Exportable{
  @export String key = "";
  @export String value = "";
  @export bool proposed = false;
  @export String lcDocId = "";

  Map convertToMap(){
    return this.toMap();
  }
}