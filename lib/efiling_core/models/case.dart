import 'package:eno_dart_components_lib/efiling_core/models/caption.dart';
import 'package:eno_dart_components_lib/efiling_core/models/case_number.dart';
import 'package:eno_dart_components_lib/efiling_core/models/court.dart';
import 'package:eno_dart_components_lib/efiling_core/models/court_view_id.dart';
import 'package:eno_dart_components_lib/efiling_core/models/document.dart';
import 'package:eno_dart_components_lib/efiling_core/models/domain.dart';
import 'package:eno_dart_components_lib/efiling_core/models/party.dart';
import 'package:eno_dart_components_lib/efiling_core/models/payment.dart';
import 'package:eno_dart_components_lib/efiling_core/models/portal_user.dart';
import 'package:eno_dart_components_lib/efiling_core/models/simple_filer.dart';
import 'package:exportable/exportable.dart';


class Case extends Domain {
  @export CourtViewId courtViewId;
  @export CaseNumber caseNumber;
  @export String caseType;
  @export String caseDescription;
  @export Caption caption;
  @export String courtViewCaseCode; // courtView caseCode
  @export int courtViewSeq; // courtView Case Seq number
  @export SimpleFiler filer;
  @export PortalUser createdBy;
  @export DateTime fileDate;
  @export String faxNumber;
  @export bool fax;
  @export String supportIssues;
  @export String relatedCases;
  @export Court court;
  @export Payment payment;
  @export List<Party> otherParties = new List<Party>();
  @export List<Party> parties = new List<Party>();
//  @export List<DocketEntry> docketEntries = new List<DocketEntry>();
//  @export List<Docket> dockets = new List<Docket>();
  @export List<Document> documents = new List<Document>();
  @export List<int> courtViewCaseIds = new List<int>();
//  @export String status = CaseStatusEnum.OPEN.toString();

  Case convertFromJson(Map caseMap){
    Case caseObj = new Case();
    caseObj.initFromMap(caseMap);
    caseObj.courtViewId = caseObj.courtViewId.convertFromJson(caseMap['courtViewId']);
    caseObj.caseNumber = caseObj.caseNumber.convertFromJson(caseMap['caseNumber']);
    caseObj.caption = caseObj.caption.convertFromJson(caseMap['caption']);
    caseObj.filer = caseObj.filer.convertFromJson(caseMap['filer']);
    caseObj.createdBy = caseObj.createdBy.convertFromJson(caseMap['createdBy']);
    caseObj.court = caseObj.court.convertFromJson(caseMap['court']);
    caseObj.payment = caseObj.payment.convertFromJson(caseMap['payment']);
    caseObj.createdBy = caseObj.createdBy.convertFromJson(caseMap['createdBy']);
    caseObj.payment = caseObj.payment.convertFromJson(caseMap['payment']);

    List<Party> otherParties = new List<Party>();
    for(dynamic partyJson in caseMap['otherParties']){
      Party party = new Party();
      party = party.convertFromJson(partyJson);
      otherParties.add(party);
    }
    caseObj.otherParties = otherParties;

    List<Party> parties = new List<Party>();
    for(dynamic partyJson in caseMap['parties']){
      Party party = new Party();
      party = party.convertFromJson(partyJson);
      parties.add(party);
    }
    caseObj.parties = parties;

    List<Document> documents = new List<Document>();
    for(dynamic documentJson in caseMap['documents']){
      Document document = new Document();
      document = document.convertFromJson(documentJson);
      documents.add(document);
    }
    caseObj.documents = documents;
    return caseObj;
  }

}
