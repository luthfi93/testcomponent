import 'package:eno_dart_components_lib/efiling_core/models/address.dart';
import 'package:eno_dart_components_lib/efiling_core/models/domain.dart';
import 'package:eno_dart_components_lib/efiling_core/models/person.dart';
import 'package:exportable/exportable.dart';

class ServiceContact extends Domain {

  @export Person person = new Person();
  @export Address address = new Address();
  @export String serviceContactId = "";
  @export int officeId = 0;
  @export String officeName = "";
  @export String addByFirmName = "";
  @export String firmId = "";
  @export String casePartyId = "";
  @export String serviceStatus = "";
  @export bool opened = false;
  @export String servedDate = "";
  @export String administrativeCopy = "";
  @export bool publicServiceContact = false;


  Map convertToMap(){
    Map serviceContactMap = this.toMap();

    serviceContactMap['person'] = this.person.convertToMap();
    serviceContactMap['address'] = this.address.convertToMap();

    return serviceContactMap;
  }

  ServiceContact convertFromJson(Map serviceContactMap){
    ServiceContact serviceContact = new ServiceContact();
    serviceContact.initFromMap(serviceContactMap);

    serviceContact.person = serviceContact.person.convertFromJson(serviceContactMap['person']);
    serviceContact.address = serviceContact.address.convertFromJson(serviceContactMap['address']);
    return serviceContact;
  }

}