import 'package:eno_dart_components_lib/efiling_core/models/address.dart';
import 'package:eno_dart_components_lib/efiling_core/models/office.dart';
import 'package:exportable/exportable.dart';

class ClerkLocation extends Office {
  @export bool detailView = false;
  @export String clerkLocationClass = "";

  convertFromJson(clerkLocationJson){
    ClerkLocation clerkLocation = new ClerkLocation();
    clerkLocation.initFromMap(clerkLocationJson);

    Address address = new Address();
    address.initFromMap(clerkLocationJson['address']);

    clerkLocation.address = address;
    return clerkLocation;
  }

}