import 'package:eno_dart_components_lib/efiling_core/models/attorney.dart';
import 'package:eno_dart_components_lib/efiling_core/models/disposition.dart';
import 'package:eno_dart_components_lib/efiling_core/models/document.dart';
import 'package:eno_dart_components_lib/efiling_core/models/domain.dart';
import 'package:eno_dart_components_lib/efiling_core/models/lineitem.dart';
import 'package:eno_dart_components_lib/efiling_core/models/party.dart';
import 'package:eno_dart_components_lib/efiling_core/models/rjo_docket_text.dart';
import 'package:exportable/exportable.dart';

class GWTDocket extends Domain {
  @export String docketDate = "";
  @export List<LineItem> lineItems = new List<LineItem>();
  @export List<Document> documents = new List<Document>();
  @export String rawDocketText = "";
  @export Attorney attorney = new Attorney();
  @export String courtText = "";
  @export Domain courtSigner = new Domain();
  @export DateTime efiledDate = null;
  @export bool filingToRjo = false;
  @export String eFiledDateInitial = "";
  @export String processedDateInitial = "";
  @export String printCaptureProcessedDateInitial = "";
  @export String dispositionOptionText = "";
  @export Disposition disposition = new Disposition();
  @export RjoDocketText rjoDocketText = new RjoDocketText();
  @export List<Party> parties = new List<Party>();
  @export bool freeFiling = false;
  @export int courtViewId = 0;

  Map convertToMap(){
    Map gwtDocketMap = this.toMap();

    //convert lineItems object list to map list
    List<Map> lineItemMapList = new List<Map>();
    this.lineItems.forEach((LineItem lineItem){
      lineItemMapList.add(lineItem.convertToMap());
    });
    gwtDocketMap['lineItems'] = lineItemMapList;

    //convert documents object list to map list
    List<Map> documentsMapList = new List<Map>();
    this.documents.forEach((Document document){
      documentsMapList.add(document.convertToMap());
    });
    gwtDocketMap['documents'] = documentsMapList;

    gwtDocketMap['attorney'] = this.attorney.convertToMap();
    gwtDocketMap['rjoDocketText'] = this.rjoDocketText.convertToMap();

    //convert party object list to map list
    List<Map> partyMapList = new List<Map>();
    this.parties.forEach((Party party){
      partyMapList.add(party.convertToMap());
    });
    gwtDocketMap['parties'] = partyMapList;

    return gwtDocketMap;
  }

  GWTDocket convertFromJson(Map gwtDocketMap){
    GWTDocket gwtDocket = new GWTDocket();
    gwtDocket.initFromMap(gwtDocketMap);
    return gwtDocket;
  }

}
