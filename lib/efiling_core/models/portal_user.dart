import 'package:eno_dart_components_lib/efiling_core/models/domain.dart';
import 'package:exportable/exportable.dart';

class PortalUser extends Domain {

  static final String AUTH_METHOD_INT = "INTERNAL";
  static final String AUTH_METHOD_EXT = "EXTERNAL";
  @export String login;
  @export String portalGroup;
  @export String principal;
  @export String authMethod;
  @export String password;
  @export String repeatPassword;

  PortalUser convertFromJson(Map portalUserMap){
    PortalUser portalUser = new PortalUser();
    portalUser.initFromMap(portalUserMap);
    return portalUser;
  }

}