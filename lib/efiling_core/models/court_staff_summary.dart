import 'package:exportable/exportable.dart';
import 'package:eno_dart_components_lib/efiling_core/models/domain.dart';

class CourtStatusSummary extends Domain {
  @export int workQueue = 0;
  @export int incomingQueue = 0;
}