import 'package:eno_dart_components_lib/efiling_core/models/domain.dart';
import 'package:exportable/exportable.dart';

class Charge extends Domain {
  @export int caseId = 0;
  @export int sequenceId = 0;
  @export int chargeNumber = 0;
  @export String actionCode = "";
  @export String actionDescription = "";
  @export String chargeDescription = "";
  @export String ticketNumber = "";
  @export DateTime offenseDate;
  @export DateTime arrestDate;
  @export int speed = 0;
  @export String zoneSpeedLimit = "";
  @export String offenseDateString = "";
  @export String arrestDateString = "";
  @export String felonyDegreeCode = "";

  Map convertToMap(){
    return this.toMap();
  }

  Charge convertFromJson(chargeJson){
    Charge charge = new Charge();
    charge.initFromMap(chargeJson);
    return charge;
  }
}