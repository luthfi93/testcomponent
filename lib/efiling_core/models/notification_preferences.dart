import 'dart:collection';
import 'package:eno_dart_components_lib/efiling_core/models/domain.dart';
import 'package:exportable/exportable.dart';

class NotificationPreferences extends Domain {

  static String ACCEPTED = "ACCEPTED";
  static String REJECTED = "REJECTED";
  static String SUBMITTED = "SUBMITTED";
  static String SERVICEUNDELIVERABLE = "SERVICEUNDELIVERABLE";
  static String SUBMISSIONFAILED = "SUBMISSIONFAILED";
  static String RECEIPTED = "RECEIPTED";


  static final List<String> NOTIFICATION_CODE_TYPES = [ ACCEPTED,
  REJECTED, SUBMITTED, SERVICEUNDELIVERABLE, SUBMISSIONFAILED, RECEIPTED ];

  @export HashMap<String, bool> notificationCodes = new HashMap<String, bool>();

  bool filingAccepted = false;
  bool filingRejected = false;
  bool filingSubmitted = false;
  bool serviceUndeliverable = false;
  bool filingSubmissionFailed = false;
  bool filingReceipted = false;

}