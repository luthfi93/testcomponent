import 'package:eno_dart_components_lib/efiling_core/models/domain.dart';
import 'package:exportable/exportable.dart';

class SuffixCode extends Domain {
  @export String suffixCode = "";
  @export String suffixDescription = "";

  Map convertToMap(){
    return this.toMap();
  }
}