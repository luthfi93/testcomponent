import 'package:eno_dart_components_lib/efiling_core/models/lawfirm.dart';
import 'package:exportable/exportable.dart';

class LawFirmRegister extends Object with Exportable{
  @export LawFirm lawfirm = new LawFirm();
  @export String encryptAdminPassword = "";
  @export String passwordResetQuestion= "";
  @export String passwordResetAnswer = "";
}
