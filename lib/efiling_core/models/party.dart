import 'package:eno_dart_components_lib/efiling_core/models/address.dart';
import 'package:eno_dart_components_lib/efiling_core/models/alias.dart';
import 'package:eno_dart_components_lib/efiling_core/models/attorney.dart';
import 'package:eno_dart_components_lib/efiling_core/models/charge.dart';
import 'package:eno_dart_components_lib/efiling_core/models/court_view_id.dart';
import 'package:eno_dart_components_lib/efiling_core/models/officer.dart';
import 'package:eno_dart_components_lib/efiling_core/models/person.dart';
import 'package:eno_dart_components_lib/efiling_core/models/sentence.dart';
import 'package:eno_dart_components_lib/efiling_core/models/service_address.dart';
import 'package:eno_dart_components_lib/efiling_core/models/service_method.dart';
import 'package:exportable/exportable.dart';

class Party extends Person {
  @export CourtViewId courtViewId = new CourtViewId();
  @export int affiliationId = 0;
  @export int newId = 0;
  @export int caseId = 0;
  @export String companyName = "";
  @export String partyCode = "";
  @export String partyCodeDesc = "";
  @export String seq = "";
  @export String partyType = "";
  @export String affiliationCode = "";
  @export bool asCompany = false;
  @export bool isNewParty = false;
  @export bool fileAppearance = false;
  @export String fileAppearanceServiceType = "";
  @export bool represented = false;
  @export bool onBehalfOf = false;
  @export bool isLongPartyName = false;

  @export List<ServiceAddress> serviceAddresses = new List<ServiceAddress>();
  @export List<Address> addresses = new List<Address>();
  @export List<Attorney> representatives = new List<Attorney>();
  @export List<Party> partyAffilations = new List<Party>();
  @export List<Charge> charges = new List<Charge>();
  @export List<Sentence> sentences = new List<Sentence>();

  @export Officer officer = new Officer();
  @export List<Alias> aliases = new List<Alias>();

  @export String billingCode = "";
  @export String partyProposedCode = "";

  static final Plaintiff = "Plaintiff";
  static final Defendant = "Defendant";
  static final Proposed = "Proposed";
  static final Other = "Other";
  static final Petitioner = "Petitioner";
  static final Respondent = "Respondent";
  static final CriminalTraffDefendant = "CriminalTraffDefendant";
  static final Complaintant = "Complaintant";
  static final ProsecutingAttorney = "ProsecutingAttorney";

  String getDisplayName(){
    String displayName = "";
    displayName += firstName;

    if (this.middleInitial != null && this.middleInitial != ""){
      displayName += " " + this.middleInitial ;
    }
    if (this.lastName!= null && this.lastName != ""){
      displayName += " " + this.lastName;
    }
    if (this.namePrefix!= null && this.namePrefix != ""){
      displayName += " " + this.namePrefix + "\n\n";
    }
    return displayName;
  }

  String getAddressDisplay(){
    String addressDisplay = "";
    if(serviceAddresses.isNotEmpty){
      ServiceAddress address = serviceAddresses.first;
      addressDisplay += address.line1 + "\n" + address.line2 + "\n";
      if(address.line3 != ""){
        addressDisplay += address.line3 + "\n";
      }
      addressDisplay += address.city + ", " + address.state + " " + address.zip + "\n";
      if(address.serviceMethods.isNotEmpty){
        addressDisplay += "\n";
        for(ServiceMethod serviceMethod in address.serviceMethods){
          addressDisplay += serviceMethod.value + ", ";
        }
      }
    }
    return addressDisplay;
  }


  Map convertToMap(){
    Map partyMap = this.toMap();

    //convert service address object list to map list
    List<ServiceAddress> serviceAddresses = this.serviceAddresses;
    List<dynamic> serviceAddressMapList = new List<dynamic>();
    serviceAddresses.forEach((ServiceAddress serviceAddress){
      serviceAddressMapList.add(serviceAddress.convertToMap());
    });
    partyMap['serviceAddresses'] = serviceAddressMapList;

    //convert address object list to map list
    List<dynamic> addressMapList = new List<dynamic>();
    this.addresses.forEach((Address address){
      addressMapList.add(address.convertToMap());
    });
    partyMap['addresses'] = addressMapList;

    //convert representatives object list to map list
    List<dynamic> representativeMapList = new List<dynamic>();
    this.representatives.forEach((Attorney attorney){
      representativeMapList.add(attorney.convertToMap());
    });
    partyMap['representatives'] = representativeMapList;

    //convert partyAffilations object list to map list
    List<Map> partyAffilationsMapList = new List<Map>();
    this.partyAffilations.forEach((Party partyAffilation){
      partyAffilationsMapList.add(partyAffilation.convertToMap());
    });
    partyMap['partyAffilations'] = partyAffilationsMapList;

    //convert charges object list to map list
    List<Map> chargesMapList = new List<Map>();
    this.charges.forEach((Charge charge){
      chargesMapList.add(charge.convertToMap());
    });
    partyMap['charges'] = chargesMapList;

    //convert sentences object list to map list
    List<Map> sentencesMapList = new List<Map>();
    this.sentences.forEach((Sentence sentense){
      sentencesMapList.add(sentense.convertToMap());
    });
    partyMap['sentences'] = sentencesMapList;

    //convert aliases object list to map list
    List<Map> aliasesMapList = new List<Map>();
    this.aliases.forEach((Alias alias){
      aliasesMapList.add(alias.convertToMap());
    });
    partyMap['aliases'] = aliasesMapList;

    return partyMap;
  }

  Party convertFromJSON(partyJson){
    Party party = new Party();
    party.initFromMap(partyJson);

    CourtViewId courtViewId = new CourtViewId();
    if(partyJson['courtViewId'] != null){
      courtViewId.initFromMap(partyJson['courtViewId']);
    }
    party.courtViewId = courtViewId;

    List<ServiceAddress> serviceAddresses = new List<ServiceAddress>();
    if(partyJson['serviceAddresses'] != null){
      for(dynamic serviceAddressJson in partyJson['serviceAddresses']){
        ServiceAddress serviceAddress = new ServiceAddress();
        serviceAddress = serviceAddress.convertFromJson(serviceAddressJson);
        serviceAddresses.add(serviceAddress);
      }
    }
    party.serviceAddresses = serviceAddresses;

    List<Address> addresses = new List<Address>();
    if(partyJson['addresses'] != null){
      for(dynamic addressJson in partyJson['addresses']){
        Address address = new Address();
        address = address.convertFromJson(addressJson);
        addresses.add(address);
      }
    }
    party.addresses = addresses;


    List<Attorney> representatives = new List<Attorney>();
    if(partyJson['representatives'] != null){
      for(dynamic representativeJson in partyJson['representatives']){
        Attorney attorney = new Attorney();
        attorney = attorney.convertFromJson(representativeJson);
        representatives.add(attorney);
      }
    }
    party.representatives = representatives;

    List<Party> partyAffilations = new List<Party>();
    if(partyJson['partyAffilations'] != null){
      for(dynamic partyAffilationsJson in partyJson['partyAffilations']){
        Party party = new Party();
        party = party.convertFromJson(partyAffilationsJson);
        partyAffilations.add(party);
      }
    }
    party.representatives = representatives;

    List<Charge> charges = new List<Charge>();
    if(partyJson['charges'] != null){
      for(dynamic chargesJson in partyJson['charges']){
        Charge charge = new Charge();
        charge = charge.convertFromJson(chargesJson);
        charges.add(charge);
      }
    }
    party.charges = charges;

    List<Sentence> sentences = new List<Sentence>();
    if(partyJson['sentences'] != null){
      for(dynamic sentenceJson in partyJson['sentences']){
        Sentence sentence = new Sentence();
        sentence = sentence.convertFromJson(sentenceJson);
        sentences.add(sentence);
      }
    }
    party.sentences = sentences;

    Officer officer = new Officer();
    if(partyJson['officer'] != null){
      officer = officer.convertFromJson(partyJson['officer']);
    }
    party.officer = officer;

    List<Alias> aliases = new List<Alias>();
    if(partyJson['aliases'] != null){
      for(dynamic aliasesJson in partyJson['aliases']){
        Alias alias = new Alias();
        alias = alias.convertFromJson(aliasesJson);
        aliases.add(alias);
      }
    }
    party.aliases = aliases;

    return party;
  }

}