import 'package:eno_dart_components_lib/efiling_core/models/credit_card.dart';
import 'package:eno_dart_components_lib/efiling_core/models/domain.dart';
import 'package:eno_dart_components_lib/efiling_core/models/filing_fee.dart';
import 'package:exportable/exportable.dart';

class BillingInfo extends Domain {

  @export List<FilingFee> filingFees = new List<FilingFee>();
  @export CreditCard defaultCreditCard = new CreditCard();
  @export List<CreditCard> creditCards = new List<CreditCard>();
  @export bool allowFreeFiling = false;
  @export String reasonForFreeFiling = "";

  Map convertToMap(){
    Map billingInfoMap = this.toMap();

    //convert partyCodeList to map list
    List<Map> filingFeesMapList = new List<Map>();
    this.filingFees.forEach((FilingFee filingFee){
      filingFeesMapList.add(filingFee.convertToMap());
    });
    billingInfoMap['filingFees'] = filingFeesMapList;

    billingInfoMap['defaultCreditCard'] = this.defaultCreditCard.convertToMap();

    //convert creditCards to map list
    List<Map> creditCardMapList = new List<Map>();
    this.creditCards.forEach((CreditCard creditCard){
      creditCardMapList.add(creditCard.convertToMap());
    });
    billingInfoMap['creditCards'] = creditCardMapList;

    return billingInfoMap;
  }
}