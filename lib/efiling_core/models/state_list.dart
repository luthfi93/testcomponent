import 'dart:convert';

import 'package:eno_dart_components_lib/efiling_core/models/domain.dart';
import 'package:eno_dart_components_lib/efiling_core/models/state.dart';
import 'package:exportable/exportable.dart';

class StateList extends Domain {
  @export List<State> stateList = new List<State>();

  convertFromJson(stateListJson){
    StateList stateListObj = new StateList();
    stateListObj.initFromJson(JSON.encode(stateListJson));

    List<State> stateList = new List<State>();

    for(dynamic stateJson in stateListJson['stateList']){
      State state = new State();
      state.initFromJson(JSON.encode(stateJson));
      stateList.add(state);
    }

    stateListObj.stateList = stateList;
    return stateListObj;
  }

}