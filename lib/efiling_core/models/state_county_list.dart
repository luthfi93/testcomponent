import 'dart:convert';
import 'package:exportable/exportable.dart';
import 'package:eno_dart_components_lib/efiling_core/models/domain.dart';
import 'package:eno_dart_components_lib/efiling_core/models/state_county.dart';

class StateCountyList extends Domain {
  @export List<StateCounty> stateCountyList = new List<StateCounty>();

  convertFromJson(stateCountyListJson){
    StateCountyList stateCountyListObj = new StateCountyList();
    stateCountyListObj.initFromJson(JSON.encode(stateCountyListJson));

    List<StateCounty> stateCountyList = new List<StateCounty>();

    for(dynamic stateCountyJson in stateCountyListJson['stateCountyList']){
      StateCounty stateCounty = new StateCounty();
      stateCounty.initFromJson(JSON.encode(stateCountyJson));
      stateCountyList.add(stateCounty);
    }

    stateCountyListObj.stateCountyList = stateCountyList;
    return stateCountyListObj;
  }

}