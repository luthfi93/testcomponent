import 'dart:convert';
import 'package:eno_dart_components_lib/efiling_core/models/domain.dart';
import 'package:eno_dart_components_lib/efiling_core/models/office.dart';
import 'package:eno_dart_components_lib/efiling_core/models/user.dart';
import 'package:exportable/exportable.dart';


class UserModule extends Domain{

  @export String login;
  @export User user = new User();
  @export Office office = new Office();
  @export bool validSubscription;
  @export int defaultAttorneyId;
  @export int defaultQueueId;
  @export String authToken;

  convertFromJson(jsonUserModule){
    UserModule userModule = new UserModule();

    User user = new User();
    userModule.initFromJson(JSON.encode(jsonUserModule));
    user.initFromJson(JSON.encode(jsonUserModule['user']));

    Office office = new Office();
    office = office.convertFromJson(jsonUserModule['office']);

    userModule.office = office;
    userModule.user = user;
    return userModule;
  }

}