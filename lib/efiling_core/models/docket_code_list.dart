import 'dart:convert';
import 'package:eno_dart_components_lib/efiling_core/models/docket_code.dart';
import 'package:eno_dart_components_lib/efiling_core/models/domain.dart';
import 'package:exportable/exportable.dart';

class DocketCodeList extends Domain{
  @export List<DocketCode> docketCodeList = new List<DocketCode>();
  dynamic docketCodeListJson;

  DocketCodeList convertFromJson(Map docketCodeListJson){
    DocketCodeList docketCodeListObject = new DocketCodeList();
    List<DocketCode> docketCodeList = new List<DocketCode>();

    docketCodeListObject.initFromJson(JSON.encode(docketCodeListJson));

    for(dynamic docketCodeJson in docketCodeListJson['docketCodeList']){
      DocketCode docketCode = new DocketCode();
      docketCode = docketCode.convertFromJson(docketCodeJson);
      docketCodeList.add(docketCode);
    }

    docketCodeListObject.docketCodeList = docketCodeList;
    docketCodeListObject.docketCodeListJson = docketCodeListJson;
    return docketCodeListObject;
  }
}