class CaseType{
  String label;
  String value;


  CaseType(this.label, this.value);
}

class CaseTypeList{
  static List<CaseType> caseTypeList = [
    new CaseType("Civil Case", "CV")
  ];
}