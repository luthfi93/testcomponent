import 'dart:convert';
import 'package:eno_dart_components_lib/efiling_core/models/address.dart';
import 'package:eno_dart_components_lib/efiling_core/models/court.dart';
import 'package:eno_dart_components_lib/efiling_core/models/court_member.dart';
import 'package:eno_dart_components_lib/efiling_core/models/court_status_summary.dart';
import 'package:eno_dart_components_lib/efiling_core/models/queue.dart';
import 'package:exportable/exportable.dart';

class CourtStaff extends CourtMember{
  @export CourtStatusSummary status;
  dynamic courtStaffJson;

  convertFromJson(courtStaffJson){
    CourtStaff courtStaff = new CourtStaff();
    courtStaff.initFromMap(courtStaffJson);

    if(courtStaffJson['status'] != null){
      CourtStatusSummary courtStatusSummary = new CourtStatusSummary();
      courtStatusSummary.initFromMap(courtStaffJson['status']);
      courtStaff.status = courtStatusSummary;
    }

    Court court = new Court();
    if(courtStaffJson['court'] != null) {
      court = court.convertFromJSON(courtStaffJson['court']);
    }
    courtStaff.court = court;

    Address address = new Address();
    if(courtStaffJson['address'] != null) {
      address.initFromMap(courtStaffJson['address']);
    }
    courtStaff.address = address;

    Queue queue = new Queue();
    if(courtStaffJson['queue'] != null) {
      queue.initFromMap(courtStaffJson['queue']);
    }
    courtStaff.queue = queue;

    courtStaff.courtStaffJson = courtStaffJson;
    return courtStaff;
  }
}