import 'package:eno_dart_components_lib/efiling_core/models/domain.dart';
import 'package:eno_dart_components_lib/efiling_core/models/suffix_code.dart';
import 'package:exportable/exportable.dart';

class SuffixCodeList extends Domain {
  @export List<SuffixCode> suffixCodeList = new List<SuffixCode>();

  SuffixCodeList convertFromJson(suffixCodeListJson){
    SuffixCodeList suffixCodeListObj = new SuffixCodeList();

    List<SuffixCode> suffixCodeList = new List<SuffixCode>();
    for(dynamic suffixCodeJson in suffixCodeListJson['suffixCodeList']){
      SuffixCode suffixCode = new SuffixCode();
      suffixCode.initFromMap(suffixCodeJson);
      suffixCodeList.add(suffixCode);
    }

    suffixCodeListObj.suffixCodeList = suffixCodeList;
    return suffixCodeListObj;
  }

}