class QueueSort {
  String label = "";

  QueueSort(this.label);

  static final defaultQueueList = [
    new QueueSort("By Default Sorting"),
    new QueueSort("By Event Date Asc"),
    new QueueSort("By Event Date Desc"),
    new QueueSort("By Case Number Asc"),
    new QueueSort("By Case Number Desc")];
}