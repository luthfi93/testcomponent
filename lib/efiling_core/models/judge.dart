import 'package:eno_dart_components_lib/efiling_core/models/court_staff.dart';
import 'package:exportable/exportable.dart';

class Judge extends CourtStaff {
  @export String judgeBarID;
}