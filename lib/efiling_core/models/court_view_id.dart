import 'package:exportable/exportable.dart';

class CourtViewId extends Object with Exportable {
  @export int id = 0;

  CourtViewId convertFromJson(Map courtViewIdJson){
    CourtViewId courtViewId = new CourtViewId();
    courtViewId.initFromMap(courtViewIdJson);
    return courtViewId;
  }
}