import 'package:eno_dart_components_lib/efiling_core/models/domain.dart';
import 'package:exportable/exportable.dart';

class FilingFee extends Domain {
  @export double amount = 0.0;
  @export double rate = 0.0;
  @export String genericCode = "";
  @export String docketCode = "";
//  @export CourtRoomCode courtRoomCode;

  Map convertToMap(){
    return this.toMap();
  }
}
