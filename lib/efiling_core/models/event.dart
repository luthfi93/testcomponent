import 'package:angular/angular.dart';
import 'package:eno_dart_components_lib/efiling_core/models/attorney.dart';
import 'package:eno_dart_components_lib/efiling_core/models/caption.dart';
import 'package:eno_dart_components_lib/efiling_core/models/case.dart';
import 'package:eno_dart_components_lib/efiling_core/models/case_number.dart';
import 'package:eno_dart_components_lib/efiling_core/models/court_view_id.dart';
import 'package:eno_dart_components_lib/efiling_core/models/domain.dart';
import 'package:eno_dart_components_lib/efiling_core/models/filing.dart';
import 'package:eno_dart_components_lib/efiling_core/models/party.dart';
import 'package:eno_dart_components_lib/efiling_core/models/route_result.dart';
import 'package:exportable/exportable.dart';
import 'package:intl/intl.dart';

class EventState {
  final _value;
  const EventState._internal(this._value);
  toString() => 'Enum.$_value';

  static const SUBMITTED_ = const EventState._internal('Submitted');
  static const FILED = const EventState._internal('FILED');
  static const PROCESSED = const EventState._internal('PROCESSED');
  static const DEFECTIVE = const EventState._internal('DEFECTIVE');
  static const InProgress = const EventState._internal('In Progress');
}

@Injectable()
class Event extends Domain {

  static final String NEW_CASE_EVENT_TYPE = "NewCaseEvent";
  static final String NEW_FILING_EVENT_TYPE = "NewFilingEvent";
  static final String SAVE_FOR_LATER_NEW_CASE_EVENT_TYPE = "SaveForLaterNewCaseEvent";
  static final String SAVE_FOR_LATER_NEW_FILING_EVENT_TYPE = "SaveForLaterNewFilingEvent";
  static final String EMERGENCY_NEW_FILING_EVENT_TYPE = "EmergencyNewFilingEvent";
  static final String ESERVICE_EVENT_TYPE = "EserviceEvent";
  static final String CLERK_FILING_EVENT_TYPE = "ClerkFilingEvent";
  static final String CLERK_PRINT_CAPTURE_EVENT_TYPE = "ClerkPrintCaptureEvent";
  static final String CLERK_PRINT_CAPTURE_ESERVICE_EVENT_TYPE = "ClerkPrintCaptureEserviceEvent";
  static final String COURT_FILING_EVENT_TYPE = "CourtFilingEvent";
  static final String COURT_FILING_SAVE_FOR_LATER_EVENT_TYPE = "SaveForLaterCourtNewFilingEvent";

  @export String eventType = "";
  @export Filing filing = new Filing();
  @export Case _case = new Case();
  @export EventState eventState;

  @export Attorney createdByAttorney = new Attorney();

  @export DateTime eventDate;

  @export List<RouteResult> routeResults = new List<RouteResult>();
  @export List<Party> parties = new List<Party>();

  @export String defectiveNote = "";

  @export CaseNumber caseNumber = new CaseNumber();
  @export String pendingCaseNumber = "";

  @export CourtViewId courtViewCaseId = new CourtViewId();

  @export String jobId = "";

  @export Caption caseCaption = new Caption();
  @export Caption caseCodeCaption = new Caption();
  @export Caption docketCaption = new Caption();
  @export Caption docketPartiesCaption = new Caption();
  @export Caption caseNumberCaption = new Caption();
  @export Caption authorCaption = new Caption();

  @export int queuedEventId = 0;

  @export bool processing;

  @export DateTime courtEventDate;

  Map convertToMap(){
    Map eventMap = this.toMap();

    eventMap['filing'] = this.filing.convertToMap();
    eventMap['createdByAttorney'] = this.createdByAttorney.convertToMap();

    //convert routeResults to map list
    List<Map> routeResults = new List<Map>();
    this.routeResults.forEach((RouteResult routeResult){
      routeResults.add(routeResult.convertToMap());
    });
    eventMap['routeResults'] = routeResults;

    //convert parties to map list
    List<Map> parties = new List<Map>();
    this.parties.forEach((Party party){
      parties.add(party.convertToMap());
    });
    eventMap['parties'] = parties;

    return eventMap;

  }

  Event convertFromJson(Map eventJson){
    Event event = new Event();
    event.initFromMap(eventJson);

    if(eventJson['eventState'] == "SUBMITTED_"){
      event.eventState = EventState.SUBMITTED_._value;
    }
    else if(eventJson['eventState'] == "FILED"){
      event.eventState = EventState.FILED._value;
    }
    else if(eventJson['eventState'] == "DEFECTIVE"){
      event.eventState = EventState.DEFECTIVE._value;
    }
    else if(eventJson['eventState'] == "InProgress"){
      event.eventState = EventState.InProgress._value;
    }
    else if(eventJson['eventState'] == "PROCESSED"){
      event.eventState = EventState.PROCESSED._value;
    }

//    print(eventJson['eventDate']);
//    event.eventDate = eventJson['eventDate'];
//    print(event.eventDate.year);
//    print(event.eventDate.month);
//    var formatter = new DateFormat.yMd().add_jm().add_jz();
//    print(formatter);
//    String formattedDate = formatter.format(eventJson['eventDate']);
//    print("Formated date...........");
//    print(formattedDate);

    if(eventJson['docketPartiesCaption'] != null){
      Caption docketPartiesCaption = new Caption();
      event.docketPartiesCaption = docketPartiesCaption.convertFromJson(eventJson['docketPartiesCaption']);
    }

    if(eventJson['authorCaption'] != null) {
      Caption authorCaption = new Caption();
      event.authorCaption = authorCaption.convertFromJson(eventJson['authorCaption']);
    }

    if(eventJson['caseCaption'] != null) {
      Caption caseCaption = new Caption();
      event.caseCaption = caseCaption.convertFromJson(eventJson['caseCaption']);
    }

    if(eventJson['caseCodeCaption'] != null) {
      Caption caseCodeCaption = new Caption();
      event.caseCodeCaption = caseCodeCaption.convertFromJson(eventJson['caseCodeCaption']);
    }

    if(eventJson['docketCaption'] != null){
      Caption docketCaption = new Caption();
      event.docketCaption = docketCaption.convertFromJson(eventJson['docketCaption']);
    }

    if(eventJson['caseNumberCaption'] != null){
      Caption caseNumberCaption = new Caption();
      event.caseNumberCaption = caseNumberCaption.convertFromJson(eventJson['caseNumberCaption']);
    }

    Attorney createdByAttorney = new Attorney();
    event.createdByAttorney = createdByAttorney.convertFromJson(eventJson['createdByAttorney']);
    Filing filing = new Filing();
    event.filing = filing.convertFromJson(eventJson['filing']);
    return event;
  }

  bool isSubmitted(){
    return this.eventState == EventState.SUBMITTED_._value;
  }

  bool isDefective(){
    return this.eventState == EventState.DEFECTIVE._value;
  }

}
