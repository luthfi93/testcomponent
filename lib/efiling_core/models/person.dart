import 'package:eno_dart_components_lib/efiling_core/models/domain.dart';
import 'package:exportable/exportable.dart';

class Person extends Domain {
  @export String lastName = "";
  @export String firstName = "";
  @export String middleInitial = "";
  @export String nameSuffix = "";
  @export String nameSuffixCode = "";
  @export String namePrefix = "";
  @export DateTime dob;
  @export String ssn = "";
  @export String akaType = "";
  @export String aka = "";
  @export String height = "";
  @export String weight = "";
  @export int age;
  @export String eyeColor = "";
  @export String gender = "";
  @export String race = "";
  @export String facialHair = "";
  @export String hairColor = "";
  @export String phone = "";
  @export String email = "";
  @export int idntId;

  Map convertToMap(){
    return this.toMap();
  }

  Person convertFromJson(Map personJson){
      Person person = new Person();
      person.initFromMap(personJson);
      return person;
  }
}