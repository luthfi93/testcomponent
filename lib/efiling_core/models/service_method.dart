import 'package:eno_dart_components_lib/efiling_core/models/court_view_id.dart';
import 'package:eno_dart_components_lib/efiling_core/models/domain.dart';
import 'package:exportable/exportable.dart';

class ServiceMethod extends Domain {
  @export String value = "";
  @export String label = "";

  @export CourtViewId courtViewId = new CourtViewId();
  @export String partySequence = "";
  @export int addressId = 0;
  @export String serviceMethodCodeDesc = "";
  bool isSelected = false;

  ServiceMethod({this.value, this.label, this.serviceMethodCodeDesc});

  static ServiceMethod EService = new ServiceMethod(value: "ESRV", label: "E-Service", serviceMethodCodeDesc: "Service by eService");
  static ServiceMethod Mail = new ServiceMethod(value: "R", label: "Mail", serviceMethodCodeDesc: "Regular Mail");
  static ServiceMethod CertifiedMail = new ServiceMethod(value: "CERT", label: "Certified Mail", serviceMethodCodeDesc: "Certified Mail");
  static ServiceMethod Sheriff_1 = new ServiceMethod(value: "ECSO_1", label: "Erie County Sheriff Personal Service", serviceMethodCodeDesc:  "Erie County Sheriff's Dept");
  static ServiceMethod Sheriff_2 = new ServiceMethod(value: "ECSO_2", label: "Erie County Sheriff Personal or Residential Service", serviceMethodCodeDesc: "Erie County Sheriff's Dept");
  static ServiceMethod ForeignSheriff_1 = new ServiceMethod(value: "FS_1", label: "Foreign Sheriff Personal Service", serviceMethodCodeDesc: "Foreign Sheriff's Dept");
  static ServiceMethod ForeignSheriff_2 = new ServiceMethod(value: "FS_2", label: "Foreign Sheriff Personal or Residential Service", serviceMethodCodeDesc: "Foreign Sheriff's Dept");
  static ServiceMethod ProcessServer_1 = new ServiceMethod(value: "P_1", label: "Process Server Personal Service", serviceMethodCodeDesc: "Process Server");
  static ServiceMethod ProcessServer_2 = new ServiceMethod(value: "P_2", label: "Process Server Personal or Residential Service", serviceMethodCodeDesc: "Process");

  static final List<ServiceMethod> serviceMethodArray = [EService, Mail, CertifiedMail, Sheriff_1, Sheriff_2, ForeignSheriff_1, ForeignSheriff_2,
  ProcessServer_1, ProcessServer_2];

  static final List<ServiceMethod> defaultServiceMethodArray = [CertifiedMail, Sheriff_1, Sheriff_2, ForeignSheriff_1, ForeignSheriff_2,
  ProcessServer_1, ProcessServer_2];

  static List<ServiceMethod> getDefaultServiceMethods(){
    for(ServiceMethod serviceMethod in defaultServiceMethodArray){
      serviceMethod.isSelected = false;
    }
    return defaultServiceMethodArray;
  }

  Map convertToMap(){
    return this.toMap();
  }

  ServiceMethod convertFromJson(Map serviceMethodMap){
    ServiceMethod serviceMethod = new ServiceMethod();
    serviceMethod.initFromMap(serviceMethodMap);

    if(serviceMethodMap != null){
      if(serviceMethodMap['courtViewId'] != null){
        serviceMethod.courtViewId = courtViewId.convertFromJson(serviceMethodMap['courtViewId']);
      }
    }
    return serviceMethod;
  }

}