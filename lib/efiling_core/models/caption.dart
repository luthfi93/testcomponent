import 'package:eno_dart_components_lib/efiling_core/models/domain.dart';
import 'package:exportable/exportable.dart';

class Caption extends Domain {
  @export String text = "";

  Caption convertFromJson(Map captionMap){
    Caption caption = new Caption();
    caption.initFromMap(captionMap);
    return caption;
  }

  Map convertToMap(){
    Map captionMap = this.toMap();
    return captionMap;
  }

}