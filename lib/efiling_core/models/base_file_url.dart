import 'package:eno_dart_components_lib/efiling_core/models/file_url.dart';
import 'package:exportable/exportable.dart';

class BaseFileUrl extends FileUrl {
  @export String path;
  @export String name;
  @export String url;

}