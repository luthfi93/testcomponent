import 'package:eno_dart_components_lib/efiling_core/models/domain.dart';
import 'package:eno_dart_components_lib/efiling_core/models/queue.dart';
import 'package:exportable/exportable.dart';

class RouteResult extends Domain {

  @export Queue routedBy = new Queue();
  @export Queue routedTo = new Queue();
  @export DateTime routeDateTime;
  @export String routeNote = "";
  @export bool archivedDefective = false;

  RouteResult convertFromJson(Map routeResultMap){
    RouteResult routeResult = new RouteResult();
    routeResult.initFromMap(routeResultMap);

    Queue routedBy = new Queue();
    routedBy = routedBy.convertFromJson(routeResultMap['routedBy']);
    routeResult.routedBy = routedBy;

    Queue routedTo = new Queue();
    routedTo = routedTo.convertFromJson(routeResultMap['routedTo']);
    routeResult.routedTo = routedTo;

    return routeResult;
  }

  Map convertToMap(){
    Map routeResultMap = this.toMap();
    if(this.routedBy != null){
      routeResultMap['routedBy'] = this.routedBy.convertToMap();
    }
    if(this.routedTo != null){
      routeResultMap['routedTo'] = this.routedTo.convertToMap();
    }
    return routeResultMap;

  }
}