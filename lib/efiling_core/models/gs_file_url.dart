import 'package:eno_dart_components_lib/efiling_core/models/base_file_url.dart';
import 'package:exportable/exportable.dart';

class GsFileUrl extends BaseFileUrl {
  @export String fileName = "";
}