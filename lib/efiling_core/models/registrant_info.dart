import 'package:eno_dart_components_lib/efiling_core/models/base_efsp_object.dart';
import 'package:exportable/exportable.dart';

class RegistrantInfo extends BaseEfspObj{
  @export String efspProviderName = "";
  @export String registerEmail = "";
  @export int memberId = 0;
  @export String firstName = "";
  @export String lastName = "";
  @export String middleName = "";
  @export int officeId = 0;
  @export String officeName = "";
  @export int userId = 0;
  @export String attorneyId = "";
  @export String role = "";
  @export String passwordResetQuestion = "";
  @export String passwordResetAnswer = "";

}