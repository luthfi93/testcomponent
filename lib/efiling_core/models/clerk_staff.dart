import 'dart:convert';
import 'package:eno_dart_components_lib/efiling_core/models/address.dart';
import 'package:eno_dart_components_lib/efiling_core/models/clerk_status_summary.dart';
import 'package:eno_dart_components_lib/efiling_core/models/office_member.dart';
import 'package:exportable/exportable.dart';

import 'package:eno_dart_components_lib/efiling_core/models/clerk_location.dart';

class ClerkStaff extends OfficeMember{

  static final String SUPERVISOR = "Supervisor";
  static final String EXECUTIVE = "Executive";
  static final String DEPUTY = "Deputy";
  static final String TITLE_CLERK_OF_COURTS = "Clerk of Courts";
  static final String TITLE_DEPUTY = "Deputy";
  static final String CASE_POOL = "Case Pool";
  static final String COURT_POOL = "Court Pool";
  static final String FILING_POOL = "Filing Pool";

  @export ClerkStatusSummary status = new ClerkStatusSummary();
  @export ClerkLocation location = new ClerkLocation();
  @export bool clerkOfCourts;
  @export String title = "";
  dynamic clerkStaffJson;


  convertFromJson(clerkStaffJson){
    ClerkStaff clerkStaff = new ClerkStaff();
    clerkStaff.initFromJson(JSON.encode(clerkStaffJson));

    ClerkStatusSummary clerkStatusSummary = new ClerkStatusSummary();
    clerkStatusSummary.initFromJson(JSON.encode(clerkStaffJson['status']));
    clerkStaff.status = clerkStatusSummary;

    ClerkLocation clerkLocation = new ClerkLocation();

    if(clerkStaffJson['clerkLocation'] != null){
      clerkLocation = clerkLocation.convertFromJson(clerkStaffJson['clerkLocation']);
    } else if(clerkStaffJson['location'] != null){
      clerkLocation = clerkLocation.convertFromJson(clerkStaffJson['location']);
    }
    clerkStaff.location = clerkLocation;

    Address address = new Address();
    address.initFromJson(JSON.encode(clerkStaffJson['address']));
    clerkStaff.address = address;

    clerkStaff.clerkStaffJson = clerkStaffJson;
    return clerkStaff;
  }

}