import 'package:eno_dart_components_lib/efiling_core/models/domain.dart';
import 'package:eno_dart_components_lib/efiling_core/models/gwt_docket.dart';
import 'package:eno_dart_components_lib/efiling_core/models/judge.dart';
import 'package:eno_dart_components_lib/efiling_core/models/party.dart';
import 'package:eno_dart_components_lib/efiling_core/models/person.dart';
import 'package:exportable/exportable.dart';

class CaseResult extends Domain {

  @export String caseNumber = "";
  @export String pendingCaseNumber = "";
  @export int caseId = 0;
  @export String caseCodeDesc = "";
  @export String status = "";
  @export String caseCaption = "";
  @export String caseType = "";
  @export String caseDescription = "";
  @export List<Person> persons = new List<Person>();
  @export List<Party> parties = new List<Party>();
  @export DateTime fileDate;
  @export String actionCode = "";
  @export Judge judge = new Judge();
  @export String judgeName = "";
  @export String judgeId = "";
  @export String magistrateName = "";
  @export String magistrateId = "";
  @export String caseTrackingId = "";
  @export List<GWTDocket> dockets = new List<GWTDocket>();


  Map convertToMap(){
    Map caseResultMap = this.toMap();

    //convert persons object list to map list
    List<Map> personsMapList = new List<Map>();
    this.persons.forEach((Person person){
      personsMapList.add(person.convertToMap());
    });
    caseResultMap['persons'] = personsMapList;

    //convert persons object list to map list
    List<Map> partiesMapList = new List<Map>();
    this.parties.forEach((Party party){
      partiesMapList.add(party.convertToMap());
    });
    caseResultMap['parties'] = partiesMapList;

    //convert dockets object list to map list
    List<Map> docketsMapList = new List<Map>();
    this.dockets.forEach((GWTDocket docket){
      docketsMapList.add(docket.convertToMap());
    });
    caseResultMap['dockets'] = docketsMapList;

    return caseResultMap;
  }

  CaseResult convertFromJson(Map caseResultMap){

    CaseResult caseResult = new CaseResult();
    caseResult.initFromMap(caseResultMap);

    List<Person> persons = new List<Person>();
    for(dynamic personMap in caseResultMap['persons']){
      Person person = new Person();
      person = person.convertFromJson(personMap);
      persons.add(person);
    }
    caseResult.persons = persons;


    List<Party> parties = new List<Party>();
    for(dynamic partiesMap in caseResultMap['parties']){
      Party party = new Party();
      party = party.convertFromJson(partiesMap);
      parties.add(party);
    }
    caseResult.parties = parties;

    List<GWTDocket> dockets = new List<GWTDocket>();
    for(dynamic docketsMap in caseResultMap['dockets']){
      GWTDocket gwtDocket = new GWTDocket();
      gwtDocket = gwtDocket.convertFromJson(docketsMap);
      dockets.add(gwtDocket);
    }
    caseResult.dockets = dockets;

    return caseResult;
  }

}