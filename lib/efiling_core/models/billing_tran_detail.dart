import 'package:eno_dart_components_lib/efiling_core/models/domain.dart';
import 'package:exportable/exportable.dart';

class BillingTranDetail extends Domain {

  @export String description = "";
  @export double amount = 0.0;
  @export String genericDocketCode = "";
  @export String cvDocketCode = "";
  @export String courtViewId = "";

  Map convertToMap(){
    return this.toMap();
  }

  BillingTranDetail convertFromJson(billingTranDetailJson){
    BillingTranDetail billingTranDetail = new BillingTranDetail();
    billingTranDetail.initFromMap(billingTranDetailJson);
    return billingTranDetail;
  }
}