import 'package:eno_dart_components_lib/efiling_core/models/billing_tran_detail.dart';
import 'package:eno_dart_components_lib/efiling_core/models/billing_tran_party.dart';
import 'package:eno_dart_components_lib/efiling_core/models/credit_card.dart';
import 'package:eno_dart_components_lib/efiling_core/models/domain.dart';
import 'package:eno_dart_components_lib/efiling_core/models/filing_fee.dart';
import 'package:exportable/exportable.dart';

class BillingTransaction extends Domain {

  @export List<BillingTranDetail> billingTranDetails = new List<BillingTranDetail>();
  @export List<BillingTranParty> billingTranParties = new List<BillingTranParty>();
  @export double totalAmount = 0.0;
  @export String attorneyBarId = "";
  @export String attorneyName = "";
  @export String firstName = "";
  @export String lastName = "";
  @export int attorneyId = 0;
  @export String billingCodes = "";
  @export String pendingCaseId = "";
  @export String pendingDocketId = "";
  @export String caseId = "";
  @export String caseNumber ="";
  @export String caseCaption = "";

  @export String orderId = "";
  @export String creditCardNumber = "";
  @export CreditCard creditCard = new CreditCard();
  @export String approvalCode = "";
  @export String referenceNumber = "";
  @export String status = "";
  @export String message = "";
  @export String timestamp = "";
  @export String docketId = "";
  @export double merchantAmount = 0.0;
  @export double convenienceFee = 0.0;
  @export double handlingFee = 0.0;

  @export bool declined = false;

  @export String note = "";

  @export String authorizationTransactionCode = "";
  @export String authorizationTransactionID = "";
  @export String gatewayTransactionID = "";

  @export String onBehalfOf = "";

  Map convertToMap(){
    Map billingTransactionMap = this.toMap();

    //convert billingTranDetails to map list
    List<Map> billingTranDetails = new List<Map>();
    this.billingTranDetails.forEach((BillingTranDetail billingTranDetail){
      billingTranDetails.add(billingTranDetail.convertToMap());
    });
    billingTransactionMap['billingTranDetails'] = billingTranDetails;

    //convert billingTranParties to map list
    List<Map> billingTranParties = new List<Map>();
    this.billingTranParties.forEach((BillingTranParty billingTranParty){
      billingTranParties.add(billingTranParty.convertToMap());
    });
    billingTransactionMap['billingTranParties'] = billingTranParties;

    return billingTransactionMap;
  }

  BillingTransaction convertFromJson(Map billingTransactionJson) {
    BillingTransaction billingTransaction = new BillingTransaction();
    billingTransaction.initFromMap(billingTransactionJson);

    //convert billingTranDetails list map to list object
    List<BillingTranDetail> billingTranDetailList = new List<BillingTranDetail>();
    for(dynamic billingTranDetailMap in billingTransactionJson['billingTranDetails']){
      BillingTranDetail billingTranDetail = new BillingTranDetail();
      billingTranDetail = billingTranDetail.convertFromJson(billingTranDetailMap);
      billingTranDetailList.add(billingTranDetail);
    }
    billingTransaction.billingTranDetails = billingTranDetailList;


    //convert billingTranParties map to object
    List<BillingTranParty> billingTranPartyList = new List<BillingTranParty>();
    for(dynamic billingTranPartyMap in billingTransactionJson['billingTranParties']){
      BillingTranParty billingTranParty = new BillingTranParty();
      billingTranParty = billingTranParty.convertFromJson(billingTranPartyMap);
      billingTranPartyList.add(billingTranParty);
    }
    billingTransaction.billingTranParties = billingTranPartyList;

    return billingTransaction;
  }
}