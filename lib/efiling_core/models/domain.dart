import 'package:eno_dart_components_lib/efiling_core/models/base_entity.dart';
import 'package:exportable/exportable.dart';

class Domain extends BaseEntity{
  @export int id = 0;
  @export String name = "";
  @export DateTime createdAt;
  @export DateTime updatedAt;
  @export String countyCode = "";
  @export String stateCode = "";
  @export String domainClassName = "";

}