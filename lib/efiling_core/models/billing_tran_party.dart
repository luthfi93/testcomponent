import 'package:eno_dart_components_lib/efiling_core/models/domain.dart';
import 'package:exportable/exportable.dart';

class BillingTranParty extends Domain {
  @export String billingCode = "";


  Map convertToMap(){
    return this.toMap();
  }

  BillingTranParty convertFromJson(billingTranPartyJson){
    BillingTranParty billingTranParty = new BillingTranParty();
    billingTranParty.initFromMap(billingTranPartyJson);
    return billingTranParty;
  }
}