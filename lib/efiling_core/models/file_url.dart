import 'package:exportable/exportable.dart';

class FileUrl extends Object with Exportable{
  @export String path = "";
  @export String name = "";
  @export String url = "";

  Map convertToMap() {
    return this.toMap();
  }

  FileUrl convertFromJson(Map fileUrlMap) {
    FileUrl fileUrl = new FileUrl();
    fileUrl.initFromMap(fileUrlMap);
    return fileUrl;
  }
}