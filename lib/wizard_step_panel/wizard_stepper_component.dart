import 'package:angular/angular.dart';
import 'package:angular/core.dart';
import 'package:angular_components/angular_components.dart';
import 'package:eno_dart_components_lib/wizard_step_panel/dynamic_stepper_directive.dart';
import 'package:eno_dart_components_lib/wizard_step_panel/wizard_step_component.dart';

@Component(
selector: 'wizard-stepper',
styleUrls: const ['wizard_stepper_component.css'],
template:"""
  <ul class="nav nav-tabs">
  <li *ngFor="let tab of tabs; let i=index" (click)="selectTab(tab)" class="li-tab cursor-pointer" [class.active-tab]="tab.active">
  <a class="tab-a" [class.tab-a-active]="tab.active">{{i+1}}. {{tab.title}}</a>
  </li>
  <!-- dynamic tabs -->
  <li *ngFor="let tab of dynamicTabs" (click)="selectTab(tab)" class="li-tab cursor-pointer" [class.active]="tab.active">
  <a class="tab-a" [class.tab-a-active]="tab.active">{{i+1}}. {{tab.title}} <span class="tab-close" *ngIf="tab.isCloseable" (click)="closeTab(tab)">x</span></a>
  </li>
  </ul>
  <ng-content></ng-content>
  <template dynamic-tabs #container></template>
  <div *ngIf="useFloatButton">
    <div *ngIf="showContinue" class="continue-button-div">
      <material-fab raised class="continue-float-button" (click)="clickNext()">
        <material-icon icon="arrow_forward"></material-icon>
      </material-fab>
    </div>
  </div>
  """,
directives: const [
  CORE_DIRECTIVES,
  materialDirectives,
  DynamicStepperDirective
],
providers: const [DynamicStepperDirective]
)

class WizardStepperComponent implements AfterContentInit {

  @Output() get continueClickEvent => clickContinue;
  EventEmitter clickContinue = new EventEmitter();

  @Output() get stepClickEvent => clickStep;
  EventEmitter clickStep = new EventEmitter();

  @Input()
  bool useFloatButton = true;

  List<WizardStepComponent> dynamicTabs = new List<WizardStepComponent>();

  @ContentChildren(WizardStepComponent)
  QueryList<WizardStepComponent> tabs = new QueryList<WizardStepComponent>();

  @ViewChild(DynamicStepperDirective)
  DynamicStepperDirective dynamicTabPlaceholder;

  ComponentResolver _componentFactoryResolver;
  int maxActiveTabIndex = 0;
  int selectedTabIndex = 0;
  bool showContinue = true;
  WizardStepComponent selectedTab;

  WizardStepperComponent(this._componentFactoryResolver, this.dynamicTabPlaceholder) {}

  ngAfterContentInit() {
    // get all active tabs
    var activeTabs = this.tabs.where((tab)=>tab.active);

    // if there is no active tab set, activate the first
    if(activeTabs.length == 0) {
      this.selectTab(this.tabs.first);
    }
  }

  openTab(String title, dynamic template, dynamic data, bool isCloseable) async {
      // get a component factory for our TabComponent
      ComponentFactory componentFactory = await _componentFactoryResolver.resolveComponent(WizardStepComponent);

      // fetch the view container reference from our anchor directive
      var viewContainerRef = dynamicTabPlaceholder.viewContainer;

      // alternatively...
      // let viewContainerRef = this.dynamicTabPlaceholder;

      // create a component instance
      var componentRef = viewContainerRef.createComponent(componentFactory);

      // set the according properties on our component instance
      WizardStepComponent instance = componentRef.instance as WizardStepComponent;
      instance.title = title;
      instance.templateBind = template;
      instance.dataContext = data;
      instance.isCloseable = isCloseable;

      // remember the dynamic component for rendering the
      // tab navigation headers
      this.dynamicTabs.add(componentRef.instance as WizardStepComponent);

      // set it active
      this.selectTab(this.dynamicTabs[this.dynamicTabs.length - 1]);
  }

  selectTab(WizardStepComponent tab){

    if(tabs.toList().indexOf(tab) <= maxActiveTabIndex){
      clickStep.add(tabs.toList().indexOf(tab) + 1);
    }
    selectedTab = tab;
  }

  setSelectTab(){
    if(tabs.toList().indexOf(selectedTab) <= maxActiveTabIndex){
        selectedTabIndex = tabs.toList().indexOf(selectedTab);
        this.tabs.toList().forEach((tab) => tab.active = false);
        this.dynamicTabs.forEach((tab) => tab.active = false);
        selectedTab.active = true;
        manageContinue();
    }
  }

  setActiveTab(index){
    if(index < tabs.length){
      selectedTabIndex = index;
      this.tabs.toList().forEach((tab) => tab.active = false);
      this.dynamicTabs.forEach((tab) => tab.active = false);
      this.tabs.toList()[index].active = true;
      manageContinue();
      if(maxActiveTabIndex < index){
        maxActiveTabIndex = index;
      }
    }
  }

  setMaxActiveTabIndex(index){
    maxActiveTabIndex = index;
  }

  manageContinue(){
    if(selectedTabIndex == (tabs.toList().length - 1)){
      showContinue = false;
    } else {
      showContinue = true;
    }
  }

  closeTab(WizardStepComponent tab) {
    for(var i=0; i<this.dynamicTabs.length;i++) {
      if(this.dynamicTabs[i] == tab) {
        // remove the tab from our array
        this.dynamicTabs.removeRange(i,1);

        // destroy our dynamically created component again
        var viewContainerRef = dynamicTabPlaceholder.viewContainer;
        // let viewContainerRef = this.dynamicTabPlaceholder;
        viewContainerRef.remove(i);

        // set tab index to 1st one
        this.selectTab(this.tabs.first);
        break;
      }
    }
  }

  closeActiveTab() {
    List<WizardStepComponent> activeTabs = this.dynamicTabs.where((tab)=>tab.active);
    if(activeTabs.length > 0)  {
      // close the 1st active tab (should only be one at a time)
      this.closeTab(activeTabs[0]);
    }
  }

  getSelectedChildComponent(){
    return tabs.toList()[selectedTabIndex].childComponent;
  }

  List getChildComponentList(){
    List childComponentList = new List();
    for(int i=0; i < maxActiveTabIndex; i++){
      childComponentList.add(tabs.toList()[i].childComponent);
    }
    return childComponentList;
  }

  clickNext(){
    clickContinue.add(selectedTabIndex+1);
  }

}