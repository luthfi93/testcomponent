import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';

@Component(
selector: 'wizard-step',
styleUrls: const ['wizard_stepper_component.css'],
template: """
<div [hidden]="!active" class="pane wizard-step-body">
<ng-content></ng-content>
<template [ngTemplateOutlet]="templateBind"></template>
</div>
""",
    directives: const [
      CORE_DIRECTIVES,
      materialDirectives
    ]
)
class WizardStepComponent implements OnInit{
  @Input('tabTitle') String title;
  @Input() bool active = false;
  @Input() bool isCloseable = false;
  @Input() dynamic templateBind;
  @Input() dynamic dataContext;
  @Input() dynamic childComponent;
  @Output() get getContinueClick => clickContinue;

  EventEmitter clickContinue = new EventEmitter();

  ngOnInit(){

  }

  clickNext(){
    clickContinue.add(true);
  }

}