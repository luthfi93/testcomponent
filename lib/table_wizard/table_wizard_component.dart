import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';

@Component(
    selector: 'table-wizard',
    styleUrls: const ['table_wizard_component.css'],
    templateUrl: 'table_wizard_component.html',
    directives: const [materialDirectives, CORE_DIRECTIVES],
    providers: const [materialProviders]
)
class TableWizardComponent {
  @Input() List<String> headerTitleList;
  @Input() bool isViewCheckBox = false;

}
