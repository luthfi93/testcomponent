import 'package:angular/angular.dart';

@Injectable()
class LoadingService {

  EventEmitter showSpinner = new EventEmitter();

  startSpinner(){
    showSpinner.add(true);
  }

  stopSpinner(){
    showSpinner.add(false);
  }

}