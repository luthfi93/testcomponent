import 'dart:async';
import 'dart:html';
import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';
import 'package:eno_dart_components_lib/loading_model/loading_service.dart';

@Component(
    selector: 'loading-model',
    styleUrls: const ['loading_component.css'],
    templateUrl: 'loading_component.html',
    directives: const [
      CORE_DIRECTIVES,
      materialDirectives
    ],
    providers: const []
)
class LoadingComponent implements OnInit, OnDestroy{
  LoadingService loadingService;
  bool showLoading = false;
  var listen;

  LoadingComponent(this.loadingService);

  @override
  ngOnInit() {
    init();
  }

  @override
  ngOnDestroy() {
    listen.cancel();
  }

  init(){
    listen = loadingService.showSpinner.listen((value){
      showLoading = value;
      setProperty();
    });
  }

  setProperty(){
    new Future.delayed(const Duration(milliseconds: 150), (){
      dynamic baseModel = document.querySelector('.loading-dialog');
      if(baseModel!=null){
        baseModel.parent.style.setProperty('z-index','9999');
      }
    });
  }
}